# Trailer Brain

Flash to a nrf52840 based device using cargo run

## Pinouts

Refer to electrical design layout in design.pdf for schematics

## Alocator

No need for an allocator as everything is statically allocated. Objects are drawn by sending generalized primitives to the drawer which creates and renders Embedded Graphics objects.
