#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

mod components;
mod datastore;
mod hardware;
mod page;
mod settings;

use components::{
    button::Button,
    buttongroup::ButtonGroup,
    drawer::{Drawables, Rectangle, UIFont},
    fuelgauge::{Fuel, FuelGauge, Orientation},
    header::Header,
    imu_canvas::ImuCanvas,
    label::Label,
    layout::layout,
    rgbled::{RGBLed, RGBMode},
    Component, Events, Location,
};
use datastore::{BatteryInfo, FuelInfo, IMUInfo, Imu, Msg, RGBInfo, Relays};
use defmt::{info, unwrap};
use display_interface_spi::SPIInterfaceNoCS;
use embassy_executor::Spawner;
use embassy_futures::{
    select::select,
    select::Either,
    select::Either::{First, Second},
};
use embassy_rp::{
    gpio::{AnyPin, Input, Level, Output, Pin, Pull},
    i2c::{self, Blocking, I2c},
    interrupt,
    peripherals::{I2C0, PIN_20, PIN_25, PIN_6, PIN_7, PIN_8, SPI0},
    pio::{PioCommon, PioPeripheral},
    relocate::RelocatedProgram,
    spi::{Async, Config, Spi},
};
use embassy_sync::{
    blocking_mutex::raw::NoopRawMutex,
    channel::{Channel, Sender},
    pubsub::PubSubChannel,
};
use embassy_time::{Delay, Duration, Timer};
use embedded_graphics::{
    pixelcolor::Rgb666,
    prelude::{DrawTarget, RgbColor, Size},
    Drawable,
};
use heapless::Vec;
use mipidsi::{models::ILI9341Rgb666, Builder, Display};
use pcf857x::{Pcf8574a, SlaveAddr};
use static_cell::StaticCell;

use page::{Page, Pages, WheelAction};
use settings::Settings::{Colors, Constants, Dims, Types};

use crate::{
    datastore::DataValue,
    hardware::{
        battery::battery_watcher,
        imu::imu_watcher,
        relays::relay_watcher,
        water::water_watcher,
        ws2815::{rgb_leds, Ws2815},
    },
};
use {defmt_rtt as _, panic_probe as _};

// Threads will update values from hardware and send to the appropriate pub/sub
// Thread will watch pub/sub and act on new messages
// After sending updated value to the pub/sub send a ui update request
// UI will request an updated value using try_next_message_pure for only those using the

// Thread messages will be CMD structs

use {defmt_rtt as _, panic_probe as _}; // global loogger

static CONTROL_CHANNEL: StaticCell<Channel<NoopRawMutex, Events, { Constants::Q_EVENT }>> =
    StaticCell::new();
static PAGES: StaticCell<
    PubSubChannel<
        NoopRawMutex,
        Pages,
        { Constants::N_SIZE },
        { Constants::N_SUBS },
        { Constants::N_PUBS },
    >,
> = StaticCell::new();
static DRAW_CHANNEL: StaticCell<Channel<NoopRawMutex, Drawables, { Constants::Q_CANVAS }>> =
    StaticCell::new();
static WAKE_CHANNEL: StaticCell<Channel<NoopRawMutex, bool, { Constants::N_SIZE }>> =
    StaticCell::new();
static RELAY_CHANNEL: StaticCell<Channel<NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>> =
    StaticCell::new();
static WATER_CHANNEL: StaticCell<Channel<NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>> =
    StaticCell::new();
static RGB_CHANNEL: StaticCell<Channel<NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>> =
    StaticCell::new();
static BATTERY_CHANNEL: StaticCell<Channel<NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>> =
    StaticCell::new();
static IMU_CHANNEL: StaticCell<Channel<NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>> =
    StaticCell::new();

#[embassy_executor::task]
async fn blinker(mut led: Output<'static, PIN_25>, interval: Duration) {
    loop {
        led.set_high();
        Timer::after(interval).await;
        led.set_low();
        Timer::after(interval).await;
    }
}

#[embassy_executor::task]
async fn click(
    mut swt: Input<'static, PIN_8>,
    sender: Sender<'static, NoopRawMutex, Events, { Constants::Q_EVENT }>,
) {
    loop {
        swt.wait_for_low().await;
        info!("Clicked! Debouncing");
        let t = Timer::after(Constants::LONG_PRESS);
        let h = swt.wait_for_high();
        let f = select(t, h);
        match f.await {
            First(_) => {
                info!("Long press, sending long press");
                sender
                    .send(Events::Wheel {
                        action: WheelAction::Long,
                    })
                    .await;
            }
            Second(_) => {
                info!("Short press, sending click");
                sender
                    .send(Events::Wheel {
                        action: WheelAction::Click,
                    })
                    .await;
            }
        }
        Timer::after(Constants::DEBOUNCE).await;
    }
}

#[embassy_executor::task]
async fn rotary(
    mut quad_a: Input<'static, PIN_6>,
    quad_b: Input<'static, PIN_7>,
    d: Duration,
    sender: Sender<'static, NoopRawMutex, Events, { Constants::Q_EVENT }>,
) {
    loop {
        quad_a.wait_for_any_edge().await;
        let r = match quad_a.is_high() == quad_b.is_high() {
            true => Events::Wheel {
                action: WheelAction::Down,
            },
            false => Events::Wheel {
                action: WheelAction::Up,
            },
        };
        info!("Rotary event {:?}", r);
        sender.send(r).await;
        Timer::after(d).await;
    }
}

fn create_home_page(
    h: Header,
    relay_states: Vec<DataValue, 4>,
    led_states: Vec<DataValue, 4>,
    s_canvas: Types::SDrawables,
    s_event: Types::SEvents,
) -> Page {
    let mut hp = Page::new();
    hp.register_elem(Component::Header { b: h });
    let l = layout(1, 5, Dims::MAIN_PANE);

    hp.register_elem(Component::FuelGauge {
        b: FuelGauge::new(
            Some(Pages::Battery),
            Fuel::Battery,
            l[0].clone(),
            Orientation::Horizontal,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    hp.register_elem(Component::FuelGauge {
        b: FuelGauge::new(
            Some(Pages::Water),
            Fuel::Water,
            l[1].clone(),
            Orientation::Horizontal,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    hp.register_elem(Component::ButtonGroup {
        b: ButtonGroup::new(
            1,
            4,
            l[2].clone(),
            Some(Pages::Buttons),
            relay_states,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    hp.register_elem(Component::ButtonGroup {
        b: ButtonGroup::new(
            1,
            2,
            l[3].clone(),
            Some(Pages::RgbLed),
            led_states,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    let imu_angle = DataValue::IMUInfo {
        f: IMUInfo::Angles {
            state: Imu::default(),
        },
    };
    hp.register_elem(Component::Label {
        b: Label::new(
            l[4].clone(),
            None,
            imu_angle,
            Some(Pages::Imu),
            s_canvas.clone(),
            s_event.clone(),
            Some(UIFont::LLabel),
        ),
    });
    hp
}

fn create_button_page(
    h: Header,
    relay_states: Vec<DataValue, 4>,
    s_canvas: Types::SDrawables,
    s_hw: Types::SHw,
) -> Page {
    let mut bp = Page::new();
    let l_button = layout(1, 4, Dims::MAIN_PANE);
    bp.register_elem(Component::Header { b: h });
    for i in 0..relay_states.len() {
        bp.register_elem(Component::Button {
            b: Button::new(
                // labels[i],
                relay_states[i].clone(),
                l_button[i].clone(),
                s_canvas.clone(),
                s_hw.clone(),
            ),
        });
    }
    drop(relay_states);
    bp
}

fn create_imu_page(h: Header, s_canvas: Types::SDrawables, s_event: Types::SEvents) -> Page {
    let mut ip = Page::new();
    let l_imu = layout(1, 5, Dims::MAIN_PANE);
    ip.register_elem(Component::Header { b: h });
    let imu_canvas_size = Size::new(l_imu[0].get_size().width, l_imu[0].get_size().height * 4);
    let loc_imu_canvas = Location::new(l_imu[0].get_point(), imu_canvas_size);
    ip.register_elem(Component::ImuCanvas {
        b: ImuCanvas::new(loc_imu_canvas, s_canvas.clone(), s_event.clone()),
    });
    let angles = DataValue::IMUInfo {
        f: IMUInfo::Angles {
            state: Imu::default(),
        },
    };
    ip.register_elem(Component::Label {
        b: Label::new(
            l_imu[4].clone(),
            None,
            angles,
            None,
            s_canvas.clone(),
            s_event.clone(),
            None,
        ),
    });
    ip
}
fn create_rgb_page(h: Header, s_canvas: Types::SDrawables, s_hw: Types::SHw) -> Page {
    let mut rgbp = Page::new();
    let loc = layout(1, 2, Dims::MAIN_PANE);
    rgbp.register_elem(Component::Header { b: h });
    rgbp.register_elem(Component::RGBLed {
        b: RGBLed::new(
            DataValue::RGBLed {
                f: RGBInfo::Led0 {
                    mode: RGBMode::Night,
                },
            },
            loc[0].clone(),
            s_canvas.clone(),
            s_hw.clone(),
        ),
    });
    rgbp.register_elem(Component::RGBLed {
        b: RGBLed::new(
            DataValue::RGBLed {
                f: RGBInfo::Led1 {
                    mode: RGBMode::Night,
                },
            },
            loc[1].clone(),
            s_canvas.clone(),
            s_hw.clone(),
        ),
    });
    // rgbp.register_elem(Component::RGBLed {
    //     b: RGBLed::new(
    //         DataValue::RGBLed {
    //             f: RGBInfo::Led2 {
    //                 mode: RGBMode::Night,
    //             },
    //         },
    //         loc[2].clone(),
    //         s_canvas.clone(),
    //         s_hw.clone(),
    //     ),
    // });
    // rgbp.register_elem(Component::RGBLed {
    //     b: RGBLed::new(
    //         DataValue::RGBLed {
    //             f: RGBInfo::Led3 {
    //                 mode: RGBMode::Night,
    //             },
    //         },
    //         loc[3].clone(),
    //         s_canvas,
    //         s_hw,
    //     ),
    // });
    rgbp
}
fn create_zero_page(h: Header, s_canvas: Types::SDrawables, s_hw: Types::SHw) -> Page {
    let mut ip = Page::new();
    let loc = layout(1, 1, Dims::MAIN_PANE);
    ip.register_elem(Component::Header { b: h });
    let b = Button::new(
        DataValue::IMUInfo { f: IMUInfo::Zero },
        loc[0].clone(),
        s_canvas,
        s_hw,
    );
    ip.register_elem(Component::Button { b });

    ip
}
fn create_water_page(h: Header, s_canvas: Types::SDrawables, s_event: Types::SEvents) -> Page {
    let mut wp = Page::new();
    let l = layout(2, 1, Dims::MAIN_PANE);
    wp.register_elem(Component::Header { b: h });
    let water_amount = DataValue::FuelInfo {
        f: FuelInfo::Water { state: 0.0 },
    };
    wp.register_elem(Component::FuelGauge {
        b: FuelGauge::new(
            None,
            Fuel::Water,
            l[0].clone(),
            Orientation::Vertical,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    wp.register_elem(Component::Label {
        b: Label::new(
            l[1].clone(),
            Some("%"),
            water_amount,
            None,
            s_canvas,
            s_event,
            None,
        ),
    });

    wp
}

fn create_battery_page(h: Header, s_canvas: Types::SDrawables, s_event: Types::SEvents) -> Page {
    let mut bp = Page::new();
    let l = layout(2, 1, Dims::MAIN_PANE);
    let l_info = layout(1, 4, l[1].clone());
    bp.register_elem(Component::Header { b: h });
    bp.register_elem(Component::FuelGauge {
        b: FuelGauge::new(
            None,
            Fuel::Battery,
            l[0].clone(),
            Orientation::Vertical,
            s_canvas.clone(),
            s_event.clone(),
        ),
    });
    bp.register_elem(Component::Label {
        b: Label::new(
            l_info[0].clone(),
            Some("%"),
            DataValue::BatteryInfo {
                f: BatteryInfo::Soc {
                    state: f32::default(),
                },
            },
            None,
            s_canvas.clone(),
            s_event.clone(),
            None,
        ),
    });
    bp.register_elem(Component::Label {
        b: Label::new(
            l_info[1].clone(),
            Some("V"),
            DataValue::BatteryInfo {
                f: BatteryInfo::Voltage {
                    state: f32::default(),
                },
            },
            None,
            s_canvas.clone(),
            s_event.clone(),
            None,
        ),
    });
    bp.register_elem(Component::Label {
        b: Label::new(
            l_info[2].clone(),
            Some("A"),
            DataValue::BatteryInfo {
                f: BatteryInfo::Current {
                    state: f32::default(),
                },
            },
            None,
            s_canvas.clone(),
            s_event.clone(),
            None,
        ),
    });
    bp.register_elem(Component::Label {
        b: Label::new(
            l_info[3].clone(),
            Some(" C"),
            DataValue::BatteryInfo {
                f: BatteryInfo::Temperature {
                    state: f32::default(),
                },
            },
            None,
            s_canvas,
            s_event,
            None,
        ),
    });

    bp
}

struct PageStruct {
    home_page: Page,
    button_page: Page,
    battery_page: Page,
    imu_page: Page,
    rgb_page: Page,
    water_page: Page,
    zero_page: Page,
}

#[embassy_executor::task(pool_size = 5)]
async fn requestor(
    name: &'static str,
    s_hw: Types::SHw,
    t_hw: Duration,
    mut r_pages: Types::RPages,
    en_pages: Vec<Pages, { Constants::N_PAGES }>,
) {
    let mut current_page = Pages::Home;
    loop {
        let t = Timer::after(t_hw);
        let r_page = r_pages.next_message_pure();

        let f = select(r_page, t);
        match f.await {
            First(page) => {
                current_page = page;
            }
            Second(..) => {
                // If not enables then wait for event from pages pubsub/global
                // Wait at duration or for new message
                // if current page is in en_pages, then request update
            }
        }
        if en_pages.contains(&current_page) {
            info!("Requesting update with worker {:?}", name);
            s_hw.send(Msg::NoActionUpdate).await;
        } else {
            info!("[Updater] - Page does not contain element for {:?}", name);
        }
    }
}

#[embassy_executor::task]
async fn dispatcher(
    rec: Types::REvents,
    canvas_sender: Types::SDrawables,
    wake_sender: Types::SWake,
    mut pages: PageStruct,
    s_pages: Types::SPages,
) {
    let mut current_page = Pages::Home;
    let mut e = Some(Events::Page { page: Pages::Home });
    loop {
        info!("Event {:?}", e);
        match e {
            None => {
                // let event = rec.recv().await;
                e = Some(rec.recv().await);
            }
            Some(event) => match event {
                Events::ReDraw { id } => {
                    match current_page {
                        Pages::Home => &pages.home_page,
                        Pages::Buttons => &pages.button_page,
                        Pages::Battery => &pages.battery_page,
                        Pages::Imu => &pages.imu_page,
                        Pages::Water => &pages.water_page,
                        Pages::RgbLed => &pages.rgb_page,
                        Pages::Zero => &pages.zero_page,
                    }
                    .draw(id);
                    e = None;
                }
                Events::Wheel { action } => {
                    wake_sender.send(true).await;
                    match current_page {
                        Pages::Home => &mut pages.home_page,
                        Pages::Buttons => &mut pages.button_page,
                        Pages::Battery => &mut pages.battery_page,
                        Pages::Imu => &mut pages.imu_page,
                        Pages::Water => &mut pages.water_page,
                        Pages::RgbLed => &mut pages.rgb_page,
                        Pages::Zero => &mut pages.zero_page,
                    }
                    .event_handler(action);
                    e = None;
                }
                Events::Page { page } => {
                    current_page = page;
                    let header = Rectangle::new(Dims::MAIN_PANE, Some(Colors::BACKGROUND), false);
                    canvas_sender
                        .send(Drawables::Rectangle { obj: header })
                        .await;
                    let r = Rectangle::new(Dims::HEADER, Some(Colors::BACKGROUND), false);
                    canvas_sender.send(Drawables::Rectangle { obj: r }).await;
                    e = Some(Events::ReDraw { id: None });
                    s_pages.publish_immediate(current_page.clone());
                }
            },
        }
    }
}
#[embassy_executor::task]
async fn backlight_control(s_wake: Types::RWake, backlight_pin: AnyPin) {
    let mut backlight = Output::new(backlight_pin, Level::Low);
    loop {
        let wake = s_wake.recv();
        let t = Timer::after(Duration::from_secs(30));
        let f = select(wake, t);
        match f.await {
            Either::First(_) => {
                backlight.set_high();
            }
            Either::Second(_) => {
                backlight.set_low();
            }
        }
    }
}

fn init_screen<'a>(
    d_spi: SPIInterfaceNoCS<Spi<'a, SPI0, Async>, Output<'a, PIN_20>>,
    reset_pin: AnyPin,
) -> Display<
    SPIInterfaceNoCS<Spi<'a, SPI0, Async>, Output<'a, PIN_20>>,
    ILI9341Rgb666,
    Output<'a, AnyPin>,
> {
    // create the ILI9486 display driver from the display interface and optional RST pin
    let reset = Output::new(reset_pin, Level::Low);
    Builder::ili9341_rgb666(d_spi)
        .with_orientation(mipidsi::Orientation::Portrait(true))
        .init(&mut Delay, Some(reset))
        .expect("Unable to create display device with ili9341 rgb666")
}

#[embassy_executor::main]
async fn main(spawner: Spawner) {
    let main_config = Default::default();
    let p = embassy_rp::init(main_config);
    // Create the channel. This can be static as well
    let led = Output::new(p.PIN_25, Level::Low);
    unwrap!(spawner.spawn(blinker(led, Duration::from_millis(500))));
    info!("Status led set up");
    let s_event = CONTROL_CHANNEL.init(Channel::new());
    let s_wake = WAKE_CHANNEL.init(Channel::new());
    let s_canvas = DRAW_CHANNEL.init(Channel::new());
    let s_relay = RELAY_CHANNEL.init(Channel::new());
    let s_water = WATER_CHANNEL.init(Channel::new());
    let s_led = RGB_CHANNEL.init(Channel::new());
    let s_battery = BATTERY_CHANNEL.init(Channel::new());
    let s_imu = IMU_CHANNEL.init(Channel::new());
    let ps_pages = PAGES.init(PubSubChannel::new());

    let backlight_pin = p.PIN_22.degrade();
    unwrap!(spawner.spawn(backlight_control(s_wake.receiver(), backlight_pin)));
    let dc = Output::new(p.PIN_20, Level::Low);
    let spitx = p.PIN_19;
    let clk = p.PIN_18;
    let mut config = Config::default();
    config.frequency = 62_000_000;
    info!("Setting up SPI at {}bps", config.frequency);
    let d_spi = Spi::new_txonly(p.SPI0, clk, spitx, p.DMA_CH0, config);
    let d_spi = SPIInterfaceNoCS::new(d_spi, dc);

    info!("Display Setup and set to black");
    let mut display = init_screen(d_spi, p.PIN_21.degrade());
    display.clear(Rgb666::BLACK).unwrap();

    let (mut pio0, sm0, sm1, sm2, sm3) = p.PIO0.split();
    let side_set = pio::SideSet::new(false, 1, false);
    const T1: u8 = 2; // start bit
    const T2: u8 = 5; // data bit
    const T3: u8 = 3; // stop bit
    let mut a: pio::Assembler<32> = pio::Assembler::new_with_side_set(side_set);
    let mut wrap_target = a.label();
    let mut wrap_source = a.label();
    let mut do_zero = a.label();
    a.set_with_side_set(pio::SetDestination::PINDIRS, 1, 0);
    a.bind(&mut wrap_target);
    // Do stop bit
    a.out_with_delay_and_side_set(pio::OutDestination::X, 1, T3 - 1, 0);
    // Do start bit
    a.jmp_with_delay_and_side_set(pio::JmpCondition::XIsZero, &mut do_zero, T1 - 1, 1);
    // Do data bit = 1
    a.jmp_with_delay_and_side_set(pio::JmpCondition::Always, &mut wrap_target, T2 - 1, 1);
    a.bind(&mut do_zero);
    // Do data bit = 0
    a.nop_with_delay_and_side_set(T2 - 1, 0);
    a.bind(&mut wrap_source);
    let prg = a.assemble_with_wrap(wrap_source, wrap_target);
    let relocated = RelocatedProgram::new(&prg);
    pio0.write_instr(relocated.origin() as usize, relocated.code());

    let leds0 = Ws2815::new(
        sm0,
        RelocatedProgram::new(&prg),
        p.PIN_14.degrade(),
        RGBInfo::Led0 {
            mode: RGBMode::Night,
        },
    );
    let leds1 = Ws2815::new(
        sm1,
        RelocatedProgram::new(&prg),
        p.PIN_15.degrade(),
        RGBInfo::Led1 {
            mode: RGBMode::Night,
        },
    );
    // let leds2 = Ws2815::new(
    //     sm2,
    //     RelocatedProgram::new(&prg),
    //     p.PIN_16.degrade(),
    //     RGBInfo::Led2 {
    //         mode: RGBMode::Night,
    //     },
    // );
    // let leds3 = Ws2815::new(
    //     sm3,
    //     RelocatedProgram::new(&prg),
    //     p.PIN_27.degrade(),
    //     RGBInfo::Led3 {
    //         mode: RGBMode::Night,
    //     },
    // );
    unwrap!(spawner.spawn(rgb_leds(
        leds0,
        leds1,
        // leds2,
        // leds3,
        s_led.receiver(),
        s_event.sender()
    )));

    let quad_a = Input::new(p.PIN_6, Pull::Up);
    let quad_b = Input::new(p.PIN_7, Pull::Up);
    unwrap!(spawner.spawn(rotary(
        quad_a,
        quad_b,
        Duration::from_millis(100),
        s_event.sender()
    )));
    info!("Rotary Encoder set up");
    let click_irq = Input::new(p.PIN_8, Pull::Up);
    unwrap!(spawner.spawn(click(click_irq, s_event.sender())));
    info!("Click interupt set up");

    // Water sample
    let mut relay_states = Vec::<DataValue, 4>::new();
    relay_states
        .push(DataValue::Relays {
            f: Relays::Stove { state: false },
        })
        .unwrap();
    relay_states
        .push(DataValue::Relays {
            f: Relays::Prep { state: false },
        })
        .unwrap();
    relay_states
        .push(DataValue::Relays {
            f: Relays::Water { state: false },
        })
        .unwrap();
    relay_states
        .push(DataValue::Relays {
            f: Relays::Fridge { state: false },
        })
        .unwrap();
    let mut led_states = Vec::<DataValue, 4>::new();
    led_states
        .push(DataValue::RGBLed {
            f: RGBInfo::Led0 {
                mode: RGBMode::Night,
            },
        })
        .unwrap();
    led_states
        .push(DataValue::RGBLed {
            f: RGBInfo::Led1 {
                mode: RGBMode::Night,
            },
        })
        .unwrap();
    let h_home = Header::new(
        None,
        Dims::HEADER.clone(),
        s_canvas.sender(),
        s_event.sender(),
    );
    let header = Header::new(
        Some(Pages::Home),
        Dims::HEADER.clone(),
        s_canvas.sender(),
        s_event.sender(),
    );

    let page_struct = PageStruct {
        home_page: create_home_page(
            h_home,
            relay_states.clone(),
            led_states.clone(),
            s_canvas.sender(),
            s_event.sender(),
        ),
        button_page: create_button_page(
            header.clone(),
            relay_states,
            s_canvas.sender(),
            s_relay.sender(),
        ),
        battery_page: create_battery_page(header.clone(), s_canvas.sender(), s_event.sender()),
        imu_page: create_imu_page(header.clone(), s_canvas.sender(), s_event.sender()),
        rgb_page: create_rgb_page(header.clone(), s_canvas.sender(), s_led.sender()),
        water_page: create_water_page(header.clone(), s_canvas.sender(), s_event.sender()),
        zero_page: create_zero_page(header, s_canvas.sender(), s_imu.sender()),
    };
    unwrap!(spawner.spawn(dispatcher(
        s_event.receiver(),
        s_canvas.sender(),
        s_wake.sender(),
        page_struct,
        ps_pages.publisher().unwrap()
    )));
    let imu_pages = Vec::<Pages, { Constants::N_PAGES }>::from_slice(&[Pages::Home, Pages::Imu])
        .expect("Failed to make imu vec");
    let relay_pages =
        Vec::<Pages, { Constants::N_PAGES }>::from_slice(&[Pages::Home, Pages::Buttons])
            .expect("Failed to make relay vec");
    let water_pages =
        Vec::<Pages, { Constants::N_PAGES }>::from_slice(&[Pages::Home, Pages::Water])
            .expect("Failed to make water vec");
    let rgb_pages = Vec::<Pages, { Constants::N_PAGES }>::from_slice(&[Pages::Home, Pages::RgbLed])
        .expect("Failed to make rgb vec");
    let battery_pages = Vec::<Pages, { Constants::N_PAGES }>::from_slice(&[
        Pages::Home,
        Pages::Water,
        Pages::Battery,
        Pages::Buttons,
        Pages::Imu,
    ])
    .expect("Unable to make battery pages vec");
    unwrap!(spawner.spawn(requestor(
        "imu",
        s_imu.sender(),
        Duration::from_millis(5000),
        ps_pages.subscriber().unwrap(),
        imu_pages
    )));
    unwrap!(spawner.spawn(requestor(
        "relay",
        s_relay.sender(),
        Duration::from_secs(10),
        ps_pages.subscriber().unwrap(),
        relay_pages
    )));
    unwrap!(spawner.spawn(requestor(
        "battery",
        s_battery.sender(),
        Duration::from_secs(15),
        ps_pages.subscriber().unwrap(),
        battery_pages
    )));
    unwrap!(spawner.spawn(requestor(
        "water",
        s_water.sender(),
        Duration::from_secs(15),
        ps_pages.subscriber().unwrap(),
        water_pages
    )));
    unwrap!(spawner.spawn(requestor(
        "rgbled",
        s_led.sender(),
        Duration::from_secs(15),
        ps_pages.subscriber().unwrap(),
        rgb_pages
    )));

    let sda = p.PIN_4;
    let scl = p.PIN_5;

    let cfg = i2c::Config::default();
    // cfg.frequency = 400000;
    info!("I2C set up at rate {:?}", cfg.frequency);
    let i2c = i2c::I2c::new_blocking(p.I2C0, scl, sda, cfg);
    let bus: &'static _ = shared_bus::new_cortexm!(I2c<I2C0, Blocking> = i2c).unwrap();

    let mut expander = Pcf8574a::new(
        bus.acquire_i2c(),
        SlaveAddr::Alternative(false, false, false),
    );
    // Check initial status of outputs
    // let pins_to_be_read = PinFlag::P0
    //     | PinFlag::P1
    //     | PinFlag::P2
    //     | PinFlag::P3
    //     | PinFlag::P4
    //     | PinFlag::P5
    //     | PinFlag::P6
    //     | PinFlag::P7;
    let output_pin_status = 0b0000_0000;
    expander.set(output_pin_status).unwrap();

    // let input_state = expander.get(pins_to_be_read).unwrap();
    // info!("[Main] Got status of pin_expander as {}", input_state);
    // assert_eq!(
    //     input_state, 0x0000000,
    //     "Invalid initial state on outputs, they are not all off"
    // );
    unwrap!(spawner.spawn(relay_watcher(
        s_relay.receiver(),
        s_event.sender(),
        expander
    )));
    // Set up the imu
    unwrap!(spawner.spawn(imu_watcher(
        s_imu.receiver(),
        s_event.sender(),
        p.FLASH,
        bus.acquire_i2c()
    )));
    info!("Set up relay controller via gpio expander");
    let p_enwater = AnyPin::from(p.PIN_9);
    let irq = interrupt::take!(ADC_IRQ_FIFO);
    unwrap!(spawner.spawn(water_watcher(
        s_event.sender(),
        s_water.receiver(),
        p_enwater,
        p.PIN_26,
        p.ADC,
        irq
    )));

    let tx = p.PIN_0;
    let rx = p.PIN_1;
    // let tx_dma = p.DMA_CH2;
    // let rx_dma = p.DMA_CH1;
    let re = p.PIN_13;
    unwrap!(spawner.spawn(battery_watcher(
        s_event.sender(),
        s_battery.receiver(),
        p.UART0,
        tx,
        rx,
        // tx_dma,
        // rx_dma,
        re.degrade()
    )));
    loop {
        // Channel wait
        let c = s_canvas.recv().await;
        match c {
            Drawables::Rectangle { obj } => obj.draw(&mut display),
            Drawables::Label { obj } => obj.draw(&mut display),
            Drawables::Outline { obj } => obj.draw(&mut display),
        }
        .unwrap();
    }
}
