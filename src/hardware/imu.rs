use core::cell::RefCell;

use cortex_m::interrupt::Mutex;
use defmt::{info, warn};
use embassy_rp::{
    flash::Flash,
    i2c::{Blocking, I2c},
    peripherals::{FLASH, I2C0},
};
use heapless::Vec;
use micromath::F32;
use mma8x5x::{ic::Mma8451, mode::Active, Measurement, Mma8x5x};
use shared_bus::I2cProxy;

use crate::{
    components::Events,
    datastore::{DataValue, IMUInfo, Imu, Msg},
    settings::Settings::Types,
};

const NSAMPLES: usize = 100;
const FLASH_SIZE: usize = 1 + 8;
const VERSION: u8 = 1;
const P_OFFSET: usize = 1;
const R_OFFSET: usize = 5;
const F_OFFSET: u32 = 0_u32;

fn arr_from_slice(src: &[u8]) -> [u8; 4] {
    let o = src.try_into().unwrap();
    // let mut o = [0_u8; 4];
    // o.copy_from_slice(src);
    o
}
#[derive(Debug)]
struct Data {
    version: u8,
    zero: Imu,
}
impl Data {
    fn to_bytes(self) -> [u8; FLASH_SIZE] {
        let mut o = [0_u8; FLASH_SIZE];
        o[0] = self.version;
        let p = self.zero.pitch.to_be_bytes();
        let r = self.zero.roll.to_be_bytes();
        for i in 0..p.len() - 1 {
            o[i + P_OFFSET] = p[i];
        }
        for i in 0..r.len() - 1 {
            o[i + R_OFFSET] = r[i];
        }
        o
    }
    fn from_bytes(b: &[u8; FLASH_SIZE]) -> Self {
        let v_arr = [b[0]];
        let p_arr = arr_from_slice(&b[1..4]);
        let r_arr = arr_from_slice(&b[5..9]);
        let v = u8::from_be_bytes(v_arr);
        let p = f32::from_be_bytes(p_arr);
        let r = f32::from_be_bytes(r_arr);
        Data {
            version: v,
            zero: Imu { roll: r, pitch: p },
        }
    }
}

fn convert(zero_g: &Imu, g: Measurement) -> Imu {
    let x = F32(g.x);
    let y = F32(g.y);
    let z = F32(g.z);
    let p = z.atan2(y) * 57.3 - zero_g.pitch;
    let r = x.atan2(y) * 57.3 - zero_g.roll;
    Imu {
        roll: r.into(),
        pitch: p.into(),
    }
}
fn get_saved(flash: &mut Flash<'_, FLASH, FLASH_SIZE>) -> Imu {
    let mut buf = [0_u8; FLASH_SIZE];
    match flash.read(F_OFFSET, &mut buf) {
        Ok(..) => info!(
            "[Flash] - Succesfully read {} bytes at offset {}",
            FLASH_SIZE, F_OFFSET
        ),
        Err(e) => warn!(
            "[Flash] - Failed to read {} from offset {} with error {}",
            FLASH_SIZE, F_OFFSET, e
        ),
    }
    let o: Data = Data::from_bytes(&buf);
    // let a = 1.0;
    // a.to
    if o.version == VERSION {
        return o.zero;
    } else {
        flash.erase(F_OFFSET, F_OFFSET + FLASH_SIZE as u32).unwrap();
        let data = Data {
            version: VERSION,
            zero: Imu::default(),
        }
        .to_bytes();
        flash.write(F_OFFSET, &data).unwrap();
        return Imu::default();
    }
}
fn set_zero(flash: &mut Flash<'_, FLASH, FLASH_SIZE>, sample: Imu) {
    let d = Data {
        version: VERSION,
        zero: sample,
    };
    flash.write(F_OFFSET, &d.to_bytes()).unwrap()
}

fn smooth_imu(samples: &[Measurement]) -> Measurement {
    let mut o = Measurement::default();
    for g in samples.iter() {
        o.x += g.x;
        o.y += g.y;
        o.z += g.z;
    }
    o.x /= 100.0;
    o.y /= 100.0;
    o.z /= 100.0;
    o
}
fn get_sample(
    imu: &mut Mma8x5x<I2cProxy<Mutex<RefCell<I2c<'_, I2C0, Blocking>>>>, Mma8451, Active>,
    zero_g: &Imu,
) -> Imu {
    let mut samples: Vec<Measurement, NSAMPLES> = Default::default();
    for _ in 0..NSAMPLES {
        samples.push(imu.read().unwrap()).unwrap();
    }
    let forces = smooth_imu(&samples);
    info!(
        "[IMU] - Got x = {}, y = {}, z = {} after smoothing",
        forces.x, forces.y, forces.z
    );
    convert(&zero_g, forces)
}

#[embassy_executor::task]
pub async fn imu_watcher(
    recv: Types::RHw,
    pubs: Types::SEvents,
    f: FLASH,
    port: I2cProxy<'static, Mutex<RefCell<I2c<'static, I2C0, Blocking>>>>,
) {
    let mut imu = Mma8x5x::new_mma8451(port, mma8x5x::SlaveAddr::Alternative(true));
    info!("[IMU] - Running self test");
    imu.enable_self_test().unwrap();
    info!("[IMU] - Set to active");
    let mut imu = imu.into_active().ok().unwrap();
    let mut flash = embassy_rp::flash::Flash::<_, FLASH_SIZE>::new(f);
    // let zero = get_saved(&mut flash);
    let zero = Imu {
        // pitch: 138.26395,
        // roll: 175.16137,
        pitch: 139.5060449,
        roll: 173.70941,
    };
    info!("[IMU] - Retrieved zero as {:?}", zero);
    info!("[IMU] Set up and ready");
    loop {
        match recv.recv().await {
            Msg::NoActionUpdate => {
                info!("[IMU] Updating imu");

                let a = get_sample(&mut imu, &zero);
                info!("[IMU] - Sent {:?}", a);
                let msg = IMUInfo::Angles { state: a };
                pubs.send(Events::ReDraw {
                    id: Some(DataValue::IMUInfo { f: msg }),
                })
                .await;
            }
            Msg::State { state } => {
                if let DataValue::IMUInfo { f } = state {
                    if let IMUInfo::Zero = f {
                        info!("[IMU] - Getting and saving new zero");
                        // set_zero(&mut flash, get_sample(&mut imu, &zero));
                    }
                }
            }
            Msg::Toggle { .. } => {
                info!("[IMU] - Setting zero with toggle");

                // set_zero(&mut flash, get_sample(&mut imu, &zero));
            }
        }
    }
}
