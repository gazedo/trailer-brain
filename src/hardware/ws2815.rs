use defmt::info;
use embassy_futures::join::{self, join};
use embassy_rp::pio::{
    FifoJoin, PioInstance, PioInstanceBase, PioStateMachine, PioStateMachineInstance,
    ShiftDirection, SmInstance, SmInstanceBase,
};
use embassy_rp::relocate::RelocatedProgram;
use embassy_rp::{gpio, pio_instr_util};
use smart_leds::RGB8;

use crate::components::rgbled::RGBMode;
use crate::components::Events;
use crate::datastore::{DataValue, Msg, RGBInfo};
use crate::settings::Settings::{Colors, Constants, Types};

pub struct Ws2815<P: PioInstance, S: SmInstance> {
    sm: PioStateMachineInstance<P, S>,
    string_len: usize,
    mode: RGBMode,
    cur_mode: bool,
}

impl<P: PioInstance, S: SmInstance> Ws2815<P, S> {
    pub fn new(
        mut sm: PioStateMachineInstance<P, S>,
        prg: RelocatedProgram<32>,
        pin: gpio::AnyPin,
        led: RGBInfo,
    ) -> Self {
        // Setup sm0

        const T1: u8 = 2; // start bit
        const T2: u8 = 5; // data bit
        const T3: u8 = 3; // stop bit
        const CYCLES_PER_BIT: u32 = (T1 + T2 + T3) as u32;
        // Clock config
        const CLOCK_FREQ: u32 = 125_000_000;
        const WS2812_FREQ: u32 = 400_000;

        let bit_freq = WS2812_FREQ * CYCLES_PER_BIT;
        let mut int = CLOCK_FREQ / bit_freq;
        let rem = CLOCK_FREQ - (int * bit_freq);
        let frac = (rem * 256) / bit_freq;
        // 65536.0 is represented as 0 in the pio's clock divider
        if int == 65536 {
            int = 0;
        }

        // prepare the PIO program
        // sm.write_instr(prg.origin() as usize, prg.code());
        pio_instr_util::exec_jmp(&mut sm, prg.origin());

        // Pin config
        // let p = Output::new(pin, gpio::Level::Low);
        let out_pin = sm.make_pio_pin(pin);
        sm.set_set_pins(&[&out_pin]);
        sm.set_sideset_base_pin(&out_pin);
        sm.set_sideset_count(1);

        sm.set_clkdiv((int << 8) | frac);
        let pio::Wrap { source, target } = prg.wrap();
        sm.set_wrap(source, target);

        // FIFO config
        sm.set_autopull(true);
        sm.set_fifo_join(FifoJoin::TxOnly);
        sm.set_pull_threshold(24);
        sm.set_out_shift_dir(ShiftDirection::Left);

        sm.set_enable(true);
        let (string_len, mode) = match led {
            RGBInfo::Led0 { mode } => (Constants::LED0_LEN, mode),
            RGBInfo::Led1 { mode } => (Constants::LED1_LEN, mode),
            // RGBInfo::Led2 { mode } => (Constants::LED2_LEN, mode),
            // RGBInfo::Led3 { mode } => (Constants::LED3_LEN, mode),
        };
        Self {
            sm,
            mode,
            cur_mode: false,
            string_len,
        }
    }

    pub async fn write(&mut self, color: Option<RGB8>) {
        let c = match color {
            Some(c) => c,
            None => Colors::LED_OFF.clone(),
        };
        info!("[WS2815] - Writing <{},{},{}> to strip", c.r, c.g, c.b);
        for _ in 0..self.string_len {
            let word = (u32::from(c.g) << 24) | (u32::from(c.r) << 16) | (u32::from(c.b) << 8);
            self.sm.wait_push(word).await;
        }
    }
    pub async fn advance_mode(&mut self, color: RGBMode) -> RGBMode {
        info!(
            "Advancing rgbled mode from {} to {}",
            self.mode,
            self.mode.advance_mode()
        );
        self.mode = self.mode.advance_mode();

        self.set(self.cur_mode).await;
        color
    }
    pub async fn set(&mut self, state: bool) {
        let c = match state {
            true => Some(self.mode.get_color()),
            false => None,
        };
        self.write(c).await;
        self.cur_mode = state;
        info!("Set rgbled to {}", state);
    }
    pub fn get_mode(&self) -> RGBMode {
        self.mode.clone()
    }
    pub fn get(&self) -> bool {
        self.cur_mode.clone()
    }
}

#[embassy_executor::task]
pub async fn rgb_leds(
    mut led0: Ws2815<PioInstanceBase<0>, SmInstanceBase<0>>,
    mut led1: Ws2815<PioInstanceBase<0>, SmInstanceBase<1>>,
    // mut led2: Ws2815<PioInstanceBase<0>, SmInstanceBase<2>>,
    // mut led3: Ws2815<PioInstanceBase<0>, SmInstanceBase<3>>,
    recv: Types::RHw,
    pubs: Types::SEvents,
) {
    loop {
        match recv.recv().await {
            Msg::State { state } => {
                info!("[RGBLed] - State command {:?}", state);
                if let DataValue::RGBLed { f } = state {
                    let resp = DataValue::RGBLed {
                        f: match f {
                            RGBInfo::Led0 { mode } => RGBInfo::Led0 {
                                mode: led0.advance_mode(mode).await,
                            },
                            RGBInfo::Led1 { mode } => RGBInfo::Led1 {
                                mode: led1.advance_mode(mode).await,
                            },
                            // RGBInfo::Led2 { mode } => RGBInfo::Led2 {
                            //     mode: led2.advance_mode(mode).await,
                            // },
                            // RGBInfo::Led3 { mode } => RGBInfo::Led3 {
                            //     mode: led3.advance_mode(mode).await,
                            // },
                        },
                    };
                    pubs.send(Events::ReDraw { id: Some(resp) }).await;
                }
            }
            Msg::Toggle { field } => {
                info!("[RGBLed] - Toggle command {:?}", field);
                if let DataValue::RGBLed { f } = field {
                    let resp = DataValue::RGBLed {
                        f: match f {
                            RGBInfo::Led0 { mode: _ } => {
                                led0.set(!led0.get()).await;
                                RGBInfo::Led0 {
                                    mode: led0.get_mode(),
                                }
                            }
                            RGBInfo::Led1 { mode: _ } => {
                                led1.set(!led1.get()).await;
                                RGBInfo::Led1 {
                                    mode: led1.get_mode(),
                                }
                            } // RGBInfo::Led2 { mode: _ } => {
                              //     led2.set(!led2.get()).await;
                              //     RGBInfo::Led2 {
                              //         mode: led2.get_mode(),
                              //     }
                              // }
                              // RGBInfo::Led3 { mode: _ } => {
                              //     led3.set(!led3.get()).await;
                              //     RGBInfo::Led3 {
                              //         mode: led3.get_mode(),
                              //     }
                              // }
                        },
                    };
                    pubs.send(Events::ReDraw { id: Some(resp) }).await;
                }
            }
            Msg::NoActionUpdate => {
                info!("Sending UI update with all relay states");
                let s1 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::RGBLed {
                        f: RGBInfo::Led0 {
                            mode: led0.get_mode(),
                        },
                    }),
                });
                let s2 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::RGBLed {
                        f: RGBInfo::Led1 {
                            mode: led1.get_mode(),
                        },
                    }),
                });
                join(s1, s2).await;
                // let s3 = pubs.send(Events::ReDraw {
                //     id: Some(DataValue::RGBLed {
                //         f: RGBInfo::Led2 {
                //             mode: led2.get_mode(),
                //         },
                //     }),
                // });
                // let s4 = pubs.send(Events::ReDraw {
                //     id: Some(DataValue::RGBLed {
                //         f: RGBInfo::Led3 {
                //             mode: led3.get_mode(),
                //         },
                //     }),
                // });
                // join4(s1, s2, s3, s4).await;
            }
        }
    }
}
