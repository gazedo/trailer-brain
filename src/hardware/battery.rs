#![allow(non_snake_case)]
use cortex_m::prelude::_embedded_hal_blocking_delay_DelayUs;
use defmt::{error, info, warn};
use embassy_executor::_export::StaticCell;
use embassy_futures::block_on;
use embassy_rp::{
    gpio::{AnyPin, Level, Output},
    interrupt::{self},
    peripherals::{PIN_0, PIN_1, UART0},
    uart::{BufferedUart, Config, DataBits, Parity, StopBits},
};
use embassy_time::Delay;
use embedded_io::asynch::{Read, Write};
use heapless::Vec;
use rmodbus::{client::ModbusRequest, guess_response_frame_len, ModbusProto};

use crate::{
    components::Events,
    datastore::{BatteryInfo, Msg},
    settings::Settings::{Constants, Types},
};

macro_rules! singleton {
    ($val:expr) => {{
        type T = impl Sized;
        static STATIC_CELL: StaticCell<T> = StaticCell::new();
        let (x,) = STATIC_CELL.init(($val,));
        x
    }};
}

pub struct Register {
    reg: u16,
    size: u16,
}
pub mod RegList {
    use super::Register;

    pub const SOC: Register = Register {
        reg: 0x13b4,
        size: 2,
    };
    pub const CAPACITY: Register = Register {
        reg: 0x13b6,
        size: 2,
    };
    pub const CURRENT: Register = Register {
        reg: 0x13b2,
        size: 1,
    };
    pub const VOLTAGE: Register = Register {
        reg: 0x13b3,
        size: 1,
    };
    // pub const TEMP1: Register = Register {
    //     reg: 0x139a,
    //     size: 1,
    // };
    // pub const TEMP2: Register = Register {
    //     reg: 0x139b,
    //     size: 1,
    // };
    // pub const TEMP3: Register = Register {
    //     reg: 0x139c,
    //     size: 1,
    // };
    // pub const TEMP4: Register = Register {
    //     reg: 0x139d,
    //     size: 1,
    // };
}

const BATT_ADD: u8 = 0x30;

impl<'a> SmartBattery<'a> {
    async fn get_reg(&mut self, register: Register) -> Vec<u8, { Constants::N_BYTES }> {
        self.get_data(register.reg, register.size).await
    }
    async fn get_data(&mut self, register: u16, size: u16) -> Vec<u8, { Constants::N_BYTES }> {
        let mut mreq = ModbusRequest::new(self.address, ModbusProto::Rtu);
        let mut request: Vec<u8, { Constants::N_BYTES }> = Vec::new();
        // Send a request for data
        mreq.generate_get_holdings(register, size, &mut request)
            .unwrap();
        self.dir_pin.set_low();
        self.delay.delay_us(5_u32);
        match self.port.write_all(&request).await {
            Ok(..) => info!("[Battery] - Wrote bytes to the battery at {}", self.address),
            Err(e) => panic!("[Battery] - Got serial error {:?}", e),
        };
        self.port.flush().await.unwrap();
        self.delay.delay_us(5_u32);
        self.dir_pin.set_high();

        // Get the header of the response
        let mut buf = [0u8; 6];
        let mut read_bytes = 0_usize;
        let mut response = Vec::new();
        info!("[Battery] - Trying to read reply");
        match self.port.read_exact(&mut buf).await {
            Ok(..) => {
                read_bytes = 6;
                info!("[Battery] - Read 6 bytes in reply");
            }
            Err(e) => error!("[Battery] - Got error {:?}", e),
        }
        // for i in 0..6 {
        //     info!("Trying to get byte {}", i);
        //     let mut tmp = [0_u8];
        //     timedout = match self.port.read_exact(&mut tmp).await {
        //         Ok(..) => {
        //             buf[i] = tmp[0];
        //             false
        //         }
        //         Err(e) => {
        //             error!("[Battery] - Got error {:?}\n", e);
        //             true
        //         }
        //     };
        // }
        response.extend_from_slice(&buf).unwrap();
        // Get size of the rest of the message
        let len = guess_response_frame_len(&buf, ModbusProto::Rtu).unwrap();
        if len as usize > Constants::N_BYTES {
            panic!("[Battery] - Too large of response, will not be able to save. Expand N_BYTES constant");
        }
        if len > 6 {
            while read_bytes < len as usize {
                match self.port.read(&mut buf).await {
                    Ok(rb) => {
                        read_bytes += rb;
                        response.extend_from_slice(&buf[..rb]).unwrap();
                        info!(
                            "[Battery] - Read a total of {} bytes out of expected {}",
                            read_bytes, len
                        );
                    }
                    Err(_) => error!("[Battery] - Failed to read remainder of frame"),
                }
            }
            // let mut rest: Vec<u8, { Constants::N_BYTES }> = Vec::new();
            // timedout = match self.port.read_exact(&mut rest).await {
            //     Ok(_) => false,
            //     Err(e) => {
            //         error!("\nGot error {:?}\n", e);
            //         true
            //     }
            // };
            // response.extend(rest);
        }
        info!("[RTU Read] Final length is {:?}", len);

        mreq.parse_ok(&response).unwrap();
        return response;
    }
}

pub struct SmartBattery<'a> {
    // port: Uart<'static, UART0, Async>,
    port: BufferedUart<'a, UART0>,
    dir_pin: Output<'a, AnyPin>,
    address: u8,
    delay: Delay,
    total_capacity: f32,
}

impl<'a> SmartBattery<'a> {
    pub fn new(port: BufferedUart<'a, UART0>, dir_pin: AnyPin, delay: Delay, address: u8) -> Self {
        info!("Setting up new battery at {:?} using port UART0", address);

        let re = Output::new(dir_pin, Level::Low);
        let mut battery = SmartBattery {
            port,
            dir_pin: re,
            address,
            delay,
            total_capacity: 100.0,
        };
        battery.total_capacity = block_on(battery.get_capacity());
        return battery;
    }

    async fn get_soc(&mut self) -> f32 {
        // let temp = self.get_data(0x13b4, 2);
        let temp = self.get_reg(RegList::SOC).await;
        let arr: [u8; 4] = match temp[3..7].try_into() {
            Ok(ba) => ba,
            Err(_) => panic!("Expected a Vec of length {} but it was {}", 4, temp.len()),
        };
        let soc = u32::from_be_bytes(arr) as f32 / 1000.0;
        info!("Got new soc as {:?}", soc);
        soc
    }
    async fn get_capacity(&mut self) -> f32 {
        // let temp = self.get_data(0x13b6, 2);
        let temp = self.get_reg(RegList::CAPACITY).await;
        info!("Got capacity : [");
        for i in temp.iter() {
            info!("{}", i);
        }
        info!("]");
        // info!(
        //     "Got [{}, {}, {}, {}, {}, {}] for capacity vec",
        //     temp[0], temp[1], temp[2], temp[3], temp[4], temp[5]
        // );
        // [48, 3, 4, 0, 1, 134]
        let arr: [u8; 4] = match temp[3..7].try_into() {
            // let arr: [u8; 4] = match temp[0..3].try_into() {
            Ok(ba) => ba,
            Err(_) => panic!("Expected a Vec of length {} but it was {}", 4, temp.len()),
        };
        let capacity = u32::from_be_bytes(arr) as f32 / 1000.0;
        info!("Got new capacity as {:?}", capacity);
        capacity
    }
    async fn get_current(&mut self) -> f32 {
        // let temp = self.get_data(0x13b2, 1);
        let temp = self.get_reg(RegList::CURRENT).await;
        let arr: [u8; 2] = match temp[3..5].try_into() {
            Ok(ba) => ba,
            Err(_) => panic!("Expected a Vec of length {} but it was {}", 2, temp.len()),
        };
        let current = i16::from_be_bytes(arr) as f32 / 100.0;
        info!("Got new current {:?}", current);
        current
    }
    async fn get_voltage(&mut self) -> f32 {
        // let temp = self.get_data(0x13b3, 1);
        let temp = self.get_reg(RegList::VOLTAGE).await;
        let arr: [u8; 2] = match temp[3..5].try_into() {
            Ok(ba) => ba,
            Err(_) => panic!("Expected a Vec of length {} but it was {}", 2, temp.len()),
        };
        let voltage = u16::from_be_bytes(arr) as f32 / 10.0;
        info!("Got new voltage {:?}", voltage);
        voltage
    }
}

#[embassy_executor::task]
pub async fn battery_watcher(
    pubs: Types::SEvents,
    recv: Types::RHw,
    port: UART0,
    tx: PIN_0,
    rx: PIN_1,
    re: AnyPin,
) {
    let mut config = Config::default();
    config.baudrate = 9600;
    config.parity = Parity::ParityNone;
    config.stop_bits = StopBits::STOP1;
    config.data_bits = DataBits::DataBits8;
    let irq = interrupt::take!(UART0_IRQ);
    let tx_buf = &mut singleton!([0u8; 16])[..];
    let rx_buf = &mut singleton!([0u8; 16])[..];
    let uart = BufferedUart::new(port, irq, tx, rx, tx_buf, rx_buf, config);
    // let uart = Uart::new(port, tx, rx, tx_dma, rx_dma, config);
    // uart.blocking_write("Hello World!\r\n".as_bytes()).unwrap();
    let d = Delay;
    let mut bat = SmartBattery::new(uart, re, d, BATT_ADD);

    loop {
        let msg = recv.recv().await;
        match msg {
            Msg::NoActionUpdate => {
                info!("[Battery] - Got update request, getting data");
                let soc = BatteryInfo::Soc {
                    state: bat.get_soc().await,
                };
                let current = BatteryInfo::Current {
                    state: bat.get_current().await,
                };
                let voltage = BatteryInfo::Voltage {
                    state: bat.get_voltage().await,
                };
                // let temp = BatteryInfo::Temperature {
                //     state: bat.get_temperature(),
                // };
                info!("[Battery] - Sending updated data");
                pubs.send(Events::ReDraw {
                    id: Some(crate::datastore::DataValue::BatteryInfo { f: soc }),
                })
                .await;
                pubs.send(Events::ReDraw {
                    id: Some(crate::datastore::DataValue::BatteryInfo { f: current }),
                })
                .await;
                pubs.send(Events::ReDraw {
                    id: Some(crate::datastore::DataValue::BatteryInfo { f: voltage }),
                })
                .await;
                // pubs.send(Events::ReDraw {
                //     id: Some(crate::datastore::DataValue::BatteryInfo { f: temp }),
                // })
                // .await;
            }
            Msg::Toggle { .. } | Msg::State { .. } => warn!("Got an invalid msg type in battery"),
        }
    }
}
