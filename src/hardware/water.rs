use defmt::info;
use embassy_rp::{
    adc::{Adc, Config},
    gpio::{AnyPin, Level, Output},
    interrupt::ADC_IRQ_FIFO,
    peripherals::{ADC, PIN_26},
};
use embassy_time::{Duration, Timer};

use crate::{
    components::Events,
    datastore::{DataValue, FuelInfo, Msg},
    settings::Settings::Types,
};

const NUM_SAMPLES: usize = 100;
// Max 728
// Min 2225
// const W_MAX: f32 = 4090_f32;
const W_MAX: f32 = 1106.1898_f32;
// const W_MIN: f32 = 1965_f32;
const W_MIN: f32 = 2893.21_f32;
const W_DEN: f32 = W_MAX - W_MIN; // 1350

fn average(samples: &[u16]) -> f32 {
    let mut avg = 0_f32;
    for x in samples.iter() {
        avg += *x as f32 / NUM_SAMPLES as f32;
    }
    avg.try_into().expect("Above maximum value")

    // let sum = samples.clone().sum::<u32>();
    // sum as f32 / samples.len() as f32
}

#[embassy_executor::task]
pub async fn water_watcher(
    pubs: Types::SEvents,
    recv: Types::RHw,
    en_pin: AnyPin,
    mut pin: PIN_26,
    padc: ADC,
    irq: ADC_IRQ_FIFO,
) {
    let mut adc = Adc::new(padc, irq, Config::default());

    let mut en_water = Output::new(en_pin, Level::Low);

    loop {
        let msg = recv.recv().await;
        if let Msg::NoActionUpdate = msg {
            let mut buf = [0_u16; NUM_SAMPLES];
            en_water.set_high();
            Timer::after(Duration::from_millis(10)).await;
            for i in 0..NUM_SAMPLES {
                buf[i] = adc.read(&mut pin).await;
                Timer::after(Duration::from_millis(1)).await;
            }
            en_water.set_low();
            let adc_val = average(&buf);
            let water: f32 = 100_f32 * (adc_val - W_MIN) / W_DEN;
            // let water = 75.0_f32;
            info!("water sample: {=f32}", adc_val);
            pubs.send(Events::ReDraw {
                id: Some(DataValue::FuelInfo {
                    f: FuelInfo::Water { state: water },
                }),
            })
            .await;
        } else {
            info!("Got an invalid message in water sampler");
            continue;
        }
    }
}
