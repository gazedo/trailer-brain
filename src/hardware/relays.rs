use core::{cell::RefCell, fmt::Debug};
use cortex_m::interrupt::Mutex;
use defmt::info;
use embassy_futures::join::join4;
use embassy_rp::{
    i2c::{Blocking, I2c},
    peripherals::I2C0,
};
use pcf857x::{OutputPin, Pcf8574a};
use shared_bus::I2cProxy;

use crate::{
    components::Events,
    datastore::{DataValue, Msg, Relays},
    settings::Settings::Types,
};

struct Relay<T>
where
    T: OutputPin,
    <T as OutputPin>::Error: Debug,
{
    pin: T,
    state: bool,
}

impl<T> Relay<T>
where
    T: OutputPin,
    <T as OutputPin>::Error: Debug,
{
    pub fn new(p: T) -> Self {
        Relay {
            pin: p,
            state: false,
        }
    }
    fn set_state(&mut self, state: &bool) -> bool {
        self.state = state.clone();
        match state {
            true => self.pin.set_high(),
            false => self.pin.set_low(),
        }
        .expect("Unable to set state on relay");
        self.state
    }
    fn get_state(&self) -> bool {
        info!("Pin state is {}", self.state);
        self.state
    }
    fn toggle(&mut self) -> bool {
        self.state = match self.get_state() {
            true => {
                self.pin.set_low().expect("Unable to set Relay low");
                false
            }
            false => {
                // self.pin.set_high().expect("Unable to set Relay high");
                self.pin.set_high().unwrap();
                true
            }
        };
        self.state
    }
}
#[embassy_executor::task]
pub async fn relay_watcher(
    // Watch for a command message
    recv: Types::RHw,
    // Used to send the UI update
    pubs: Types::SEvents,
    gpio: Pcf8574a<I2cProxy<'static, Mutex<RefCell<I2c<'static, I2C0, Blocking>>>>>,
) {
    let pins = gpio.split();
    let mut fridge = Relay::new(pins.p0);
    let mut prep = Relay::new(pins.p1);
    let mut stove = Relay::new(pins.p2);
    let mut water = Relay::new(pins.p3);
    loop {
        match recv.recv().await {
            Msg::State { state } => {
                info!("Got state command for {:?}", state);
                if let DataValue::Relays { f } = state {
                    let resp = DataValue::Relays {
                        f: match f {
                            Relays::Stove { state } => Relays::Stove {
                                state: stove.set_state(&state),
                            },
                            Relays::Prep { state } => Relays::Prep {
                                state: prep.set_state(&state),
                            },
                            Relays::Fridge { state } => Relays::Fridge {
                                state: fridge.set_state(&state),
                            },
                            Relays::Water { state } => Relays::Water {
                                state: water.set_state(&state),
                            },
                        },
                    };
                    pubs.send(Events::ReDraw { id: Some(resp) }).await;
                }
            }
            Msg::Toggle { field } => {
                info!("Got toggle command for {:?}", field);
                if let DataValue::Relays { f } = field {
                    let resp = DataValue::Relays {
                        f: match f {
                            Relays::Stove { .. } => Relays::Stove {
                                state: stove.toggle(),
                            },
                            Relays::Prep { .. } => Relays::Prep {
                                state: prep.toggle(),
                            },
                            Relays::Fridge { .. } => Relays::Fridge {
                                state: fridge.toggle(),
                            },
                            Relays::Water { .. } => Relays::Water {
                                state: water.toggle(),
                            },
                        },
                    };
                    pubs.send(Events::ReDraw { id: Some(resp) }).await;
                }
            }
            Msg::NoActionUpdate => {
                info!("Sending UI update with all relay states");
                let s1 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::Relays {
                        f: Relays::Fridge {
                            state: fridge.get_state(),
                        },
                    }),
                });
                let s2 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::Relays {
                        f: Relays::Prep {
                            state: prep.get_state(),
                        },
                    }),
                });
                let s3 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::Relays {
                        f: Relays::Water {
                            state: water.get_state(),
                        },
                    }),
                });
                let s4 = pubs.send(Events::ReDraw {
                    id: Some(DataValue::Relays {
                        f: Relays::Stove {
                            state: stove.get_state(),
                        },
                    }),
                });
                join4(s1, s2, s3, s4).await;
            }
        }
    }
}
