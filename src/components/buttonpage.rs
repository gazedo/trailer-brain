use defmt::{info, warn};
use embassy_futures::block_on;
use embedded_graphics::pixelcolor::Rgb666;
use heapless::String;

use crate::{
    datastore::DataValue,
    page::Pages,
    settings::Settings::{Colors, Strings, Types},
};

use super::{
    drawer::{Drawables, RecState, Rectangle, UIFont, UILabel},
    Click, ClickableWidget, Events, Location, Widget,
};

/*
Button click for relay-
    Click button
    Change to in progress
    Send UI rebuild request for ID
    Send state to relay controller task
    Watch for confirmation of state change
    Change to Idle and Completed
    Send UI rebuild request for ID


Button click to change page
    Click button
    Change to in progress
    Send state to ui controller
    UI changes to new page
*/

#[derive(Clone)]
pub struct PageButton {
    data: Pages,
    state: RecState,
    loc: Location,
    sender: Types::SDrawables,
    event_sender: Types::SEvents,
}
impl PageButton {
    pub fn new(
        data: Pages,
        loc: Location,
        sender: Types::SDrawables,
        action_s: Types::SEvents,
    ) -> Self {
        let b = PageButton {
            data,
            state: RecState::Clear,
            loc,
            sender,
            event_sender: action_s,
        };
        b
    }
}
impl ClickableWidget for PageButton {
    // on click send the default value to the through the action_s
    fn event_handler(&mut self, event: Click) {
        self.clear_outline();
        match event {
            Click::Hover => {
                self.clear_outline();
                self.state = RecState::Hover;
                self.draw_outline();
            }
            Click::Up | Click::Long => {
                self.clear_outline();
                block_on(self.event_sender.send(Events::Page {
                    page: self.data.clone(),
                }));
            }
            Click::None => {
                self.state = RecState::Clear;
            }
        }
    }
}

impl Widget for PageButton {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn get_state(&self) -> RecState {
        self.state.clone()
    }
    fn draw(&self, _: Option<DataValue>) {
        // if the new value is of the same enum type as the default, draw
        // Check if button has description enabled and update field
        // Draw background, outline, text, and description if enabled
        warn!("Extra drawing of buttonpage");
    }
    fn draw_initial(&self) {
        info!("Drawing ButtonPage with loc {:?}", self.loc);
        let r = Rectangle::new(self.loc.clone(), Some(Colors::B_INACTIVE), true);
        block_on(self.sender.send(Drawables::Rectangle { obj: r }));
        let main_label = match &self.data {
            Pages::Home => Strings::HOME,
            Pages::Buttons => Strings::BUTTON,
            Pages::Battery => Strings::BATTERY,
            Pages::Water => Strings::WATER,
            Pages::RgbLed => Strings::RGB_LED,
            Pages::Imu => Strings::IMU,
            Pages::Zero => Strings::ZERO,
        };
        let l = UILabel::new(
            // self.loc.translate(Dims::DESC_OFFSET),
            self.loc.clone(),
            String::from(main_label),
            UIFont::PageButton,
            false,
        );
        block_on(self.sender.send(Drawables::Label { obj: l }));
        self.draw_outline();
    }
}
