use core::fmt::Write;
use defmt::info;
use embassy_futures::{block_on, join::join};
use embedded_graphics::pixelcolor::Rgb666;
use heapless::String;

use crate::{
    components::drawer::Rectangle,
    datastore::{DataValue, Deq},
    page::Pages,
    settings::Settings::{Constants, Dims, Types},
};

use super::{
    drawer::{Drawables, RecState, UIFont, UILabel},
    Click, ClickableWidget, Events, Location, Widget,
};
/*
Displays state of Something
    Watch for UI rebuild request
    On draw grab the state of Data from pub/sub
    send draw
On click:
    Send page change request if not
*/
#[derive(Clone)]
pub struct Label {
    loc: Location,
    text: String<{ Constants::L_UNIT }>,
    data: DataValue,
    page: Option<Pages>,
    state: RecState,
    sender: Types::SDrawables,
    action_sender: Types::SEvents,
    size: Option<UIFont>,
}

impl Label {
    pub fn new(
        loc: Location,
        text: Option<&'static str>,
        data: DataValue,
        page: Option<Pages>,
        sender: Types::SDrawables,
        action_sender: Types::SEvents,
        size: Option<UIFont>,
    ) -> Self {
        let temp_text = match text {
            Some(t) => String::from(t),
            None => String::new(),
        };
        let l = Label {
            loc,
            text: temp_text,
            data,
            page,
            state: RecState::Clear,
            sender,
            action_sender,
            size,
        };
        l
    }
    pub fn selectable(&self) -> bool {
        if let Some(_) = self.page {
            true
        } else {
            false
        }
    }
}
impl Widget for Label {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn get_state(&self) -> RecState {
        self.state.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn draw_initial(&self) {
        self.draw(None);
    }
    fn draw(&self, new_val: Option<DataValue>) {
        let d = match new_val {
            None => self.data.clone(),
            Some(val) => val,
        };
        if self.data.deq(&d) {
            info!("Drawing label for {:?} at {:?}", d, self.loc);
            let mut t_data: String<{ Constants::L_LABEL }> = String::new();
            info!("Created string with {} characters", t_data.capacity());
            write!(t_data, "{}{}", d, self.text).expect("Failed to fill string, data too long");
            let size = match &self.size {
                Some(s) => s,
                None => &UIFont::LLabel,
            };
            let r = Rectangle::new(self.loc.clone(), None, true);
            let l = self.loc.translate(Dims::F32_OFFSET);
            let text = UILabel::new(l, t_data, size.clone(), true);
            let s1 = self.sender.send(Drawables::Rectangle { obj: r });
            let s2 = self.sender.send(Drawables::Label { obj: text });
            block_on(join(s1, s2));
            self.draw_outline();
        }
    }
}
impl ClickableWidget for Label {
    fn event_handler(&mut self, event: Click) {
        match event {
            Click::None => {
                self.clear_outline();
                self.state = RecState::Clear;
            }
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up | Click::Long => {
                if let Some(data) = &self.page {
                    block_on(self.action_sender.send(Events::Page { page: data.clone() }));
                }
            }
        }
        self.draw_outline();
    }
}
