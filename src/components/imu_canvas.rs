use defmt::info;
use embassy_futures::block_on;
use embedded_graphics::{pixelcolor::Rgb666, prelude::Point};

use crate::{
    datastore::{DataValue, IMUInfo, Imu},
    page::Pages,
    settings::Settings::{Colors, Constants, Dims, Types},
};

use super::{
    drawer::{Drawables, RecState, Rectangle},
    Click, ClickableWidget, Events, Location, Widget,
};
/*
Displays state of IMU
    Watch for UI rebuild request
    On draw grab the state of imu from pub/sub
    send draw
On click:
    Send page change request
*/
pub struct ImuCanvas {
    loc: Location,
    state: RecState,
    sender: Types::SDrawables,
    action_sender: Types::SEvents,
}

impl ImuCanvas {
    pub fn new(loc: Location, sender: Types::SDrawables, action_sender: Types::SEvents) -> Self {
        let i = ImuCanvas {
            loc,
            state: RecState::Clear,
            sender,
            action_sender,
        };
        i
    }
    fn process_val(&self, imu_point: &Imu, outline: &Location) -> Rectangle {
        let zp = outline.get_center();
        let tl = outline.get_point();
        let br = outline.get_bottom_right();
        let s = outline.size;

        // Fit 10 degrees in either direction or 20 degrees total
        let roll_scale = s.width as f32 / 20.0;
        let pitch_scale = s.height as f32 / 20.0;

        let mut point = Point::new(
            (imu_point.roll * roll_scale + zp.x as f32) as i32,
            (imu_point.pitch * pitch_scale + zp.y as f32) as i32,
        );

        if !outline.contains(point) {
            let new_x = if point.x < tl.x {
                tl.x
            } else if point.x > br.x {
                br.x
            } else {
                point.x
            };
            let new_y = if point.y < tl.y {
                tl.y
            } else if point.y > br.y {
                br.y
            } else {
                point.y
            };
            point.x = new_x;
            point.y = new_y;
        }

        let fill_color = if (imu_point.roll < Constants::IMU_TARGET)
            && (imu_point.pitch < Constants::IMU_TARGET)
            && (imu_point.roll > (-1.0 * Constants::IMU_TARGET))
            && (imu_point.pitch > (-1.0 * Constants::IMU_TARGET))
        {
            Colors::P_GOOD
        } else {
            Colors::P_BAD
        };
        // let fill_color = if (imu_point.roll.abs() < 1.5) && (imu_point.pitch.abs() < 1.5) {
        //     Colors::P_GOOD
        // } else {
        //     Colors::P_BAD
        // };
        Rectangle::new(
            Location::new(point, Dims::IMU_PNT_SIZE),
            Some(fill_color),
            false,
        )
    }
}
impl Widget for ImuCanvas {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn get_state(&self) -> RecState {
        self.state.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn draw_initial(&self) {
        self.draw(None);
    }
    fn draw(&self, new_val: Option<DataValue>) {
        let imu: Imu = if let Some(data) = new_val {
            match data {
                DataValue::IMUInfo { f } => {
                    if let IMUInfo::Angles { state } = f {
                        state
                    } else {
                        return;
                    }
                }
                _ => return,
            }
        } else {
            Imu::default()
        };
        info!("Drawing IMUCanvas for {:?} at {:?}", imu, self.loc);
        let point = self.process_val(&imu, &self.loc);
        let canvas = Rectangle::new(self.loc.clone(), Some(Colors::IMU_CANVAS), true);
        block_on(self.sender.send(Drawables::Rectangle { obj: canvas }));
        block_on(self.sender.send(Drawables::Rectangle { obj: point }));
    }
}
impl ClickableWidget for ImuCanvas {
    fn event_handler(&mut self, event: Click) {
        match event {
            Click::None => {
                self.state = RecState::Clear;
            }
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up | Click::Long => {
                block_on(self.action_sender.send(Events::Page { page: Pages::Zero }))
            }
        }
        self.draw_outline();
    }
}
