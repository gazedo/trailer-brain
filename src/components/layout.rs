use defmt::info;
use embedded_graphics::prelude::{Point, Size};
use heapless::Vec;

use crate::components::Location;
use crate::settings::Settings::{Constants, Dims};

pub fn layout(cols: u8, rows: u8, loc: Location) -> Vec<Location, { Constants::N_CELL_ELEMENTS }> {
    let start_p = loc.get_point();
    let start_s = loc.get_size();

    let x = start_s.width as f32;
    let y = start_s.height as f32;
    let cell_size_x = x / f32::from(cols);
    let cell_size_y = y / f32::from(rows);
    let mut v = Vec::new();
    assert!(((cols * rows) as usize) <= { Constants::N_CELL_ELEMENTS });
    for x in 0..cols {
        for y in 0..rows {
            let p = Point::new(
                start_p.x + (f32::from(x) * cell_size_x + Dims::PADDING / 2.0) as i32,
                start_p.y + (f32::from(y) * cell_size_y + Dims::PADDING / 2.0) as i32,
            );
            let s = Size::new(
                (cell_size_x - Dims::PADDING) as u32,
                (cell_size_y - Dims::PADDING) as u32,
            );
            v.push(Location::new(p, s))
                .expect("Tried to push too many elements");
        }
    }
    info!(
        "Created layout with {} elements out of a max of {}",
        v.len(),
        { Constants::N_CELL_ELEMENTS }
    );
    v
}
