use defmt::info;
use embassy_futures::{block_on, join::join};
use embedded_graphics::pixelcolor::Rgb666;
use heapless::String;

use crate::{
    datastore::{DataValue, Deq, IMUInfo, Msg, Relays},
    settings::Settings::{Colors, Dims, Strings, Types},
};

use super::{
    drawer::{Drawables, RecState, Rectangle, UIFont, UILabel},
    Click, ClickableWidget, Location, Widget,
};

/*
Button click for relay-
    Click button
    Change to in progress
    Send UI rebuild request for ID
    Send state to relay controller task
    Watch for confirmation of state change
    Change to Idle and Completed
    Send UI rebuild request for ID


Button click to change page
    Click button
    Change to in progress
    Send state to ui controller
    UI changes to new page
*/

// #[derive(Clone)]
pub struct ButtonLabels<'a> {
    pub main_label: &'a str,
    pub desc_label: Option<&'a str>,
}
impl ButtonLabels<'_> {
    pub async fn loc_draw(&self, l: Location, sender: &Types::SDrawables) {
        let s1 = sender.send(Drawables::Label {
            obj: UILabel::new(
                l.translate(Dims::BUTTON_OFFSET),
                String::from(self.main_label),
                UIFont::LLabel,
                false,
            ),
        });
        if let Some(l_desc) = self.desc_label {
            let s2 = sender.send(Drawables::Label {
                obj: UILabel::new(
                    l.translate(Dims::DESC_OFFSET),
                    String::from(l_desc),
                    UIFont::SMLabel,
                    false,
                ),
            });
            block_on(join(s1, s2));
        } else {
            block_on(s1);
        }
    }
}
pub struct Button {
    data: DataValue,
    state: RecState,
    loc: Location,
    sender: Types::SDrawables,
    hw_sender: Types::SHw,
}
impl Button {
    pub fn new(
        data: DataValue,
        loc: Location,
        sender: Types::SDrawables,
        action_s: Types::SHw,
    ) -> Self {
        let b = Button {
            data,
            state: RecState::Clear,
            loc,
            sender,
            hw_sender: action_s,
        };
        b
    }
}
pub fn bool_str(state: &bool) -> &'static str {
    match state {
        true => "True",
        false => "False",
    }
}
impl ClickableWidget for Button {
    // on click send the default value to the through the action_s
    fn event_handler(&mut self, event: Click) {
        match event {
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up => {
                block_on(self.hw_sender.send(Msg::Toggle {
                    field: self.data.clone(),
                }));
            }
            Click::None => {
                self.state = RecState::Clear;
                self.clear_outline();
            }
            Click::Long => {
                block_on(self.hw_sender.send(Msg::State {
                    state: self.data.clone(),
                }));
            }
        }
        self.draw_outline();
    }
}

impl Widget for Button {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn get_state(&self) -> RecState {
        self.state.clone()
    }

    // Meant to be an update draw, IE got a new value
    fn draw(&self, val: Option<DataValue>) {
        // if the new value is of the same enum type as the default, draw
        let new_val = match val {
            Some(v) => v,
            None => {
                self.clear();
                self.data.clone()
            }
        };
        if self.data.deq(&new_val) {
            info!(
                "Drawing Button with new_val {:?} loc {:?}",
                &new_val, self.loc
            );
            let fill_color = if let DataValue::Relays { f } = &new_val {
                match f.try_get() {
                    Err(e) => panic!("Got error {:?}", e),
                    Ok(b) => match b {
                        true => Colors::B_ON,
                        false => Colors::B_OFF,
                    },
                }
            } else if let DataValue::IMUInfo { f } = &new_val {
                if let IMUInfo::Zero = f {
                    Colors::B_TEXT
                } else {
                    defmt::panic!("Got an invalid IMUInfo field in a button {:?}", new_val);
                }
            } else {
                defmt::panic!("Got an invalid datavalue field in a button {:?}", new_val);
            };

            let r = Rectangle::new(self.loc.clone(), Some(fill_color), true);
            block_on(self.sender.send(Drawables::Rectangle { obj: r }));
            let bl = match &new_val {
                DataValue::Relays { f } => match f {
                    Relays::Stove { state: s } => ButtonLabels {
                        main_label: Strings::STOVE_AREA,
                        desc_label: Some(bool_str(s)),
                    },
                    Relays::Prep { state: s } => ButtonLabels {
                        main_label: Strings::PREP_AREA,
                        desc_label: Some(bool_str(s)),
                    },
                    Relays::Fridge { state: s } => ButtonLabels {
                        main_label: Strings::FRIDGE,
                        desc_label: Some(bool_str(s)),
                    },
                    Relays::Water { state: s } => ButtonLabels {
                        main_label: Strings::WATER,
                        desc_label: Some(bool_str(s)),
                    },
                },
                DataValue::IMUInfo { f: _ } => ButtonLabels {
                    main_label: Strings::ZERO,
                    desc_label: None,
                },
                DataValue::BatteryInfo { f: _ }
                | DataValue::FuelInfo { f: _ }
                | DataValue::RGBLed { f: _ } => {
                    panic!("Invalid match arm for button");
                }
            };
            block_on(bl.loc_draw(self.loc.clone(), &self.sender));
        }
        // Check if button has description enabled and update field
        // Draw background, outline, text, and description if enabled
    }
    fn draw_initial(&self) {
        self.draw(None);
    }
}
