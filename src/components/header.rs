use defmt::info;
use embassy_futures::block_on;
use embedded_graphics::{pixelcolor::Rgb666, prelude::Point};
use heapless::String;

use crate::{
    datastore::{BatteryInfo, DataValue},
    page::Pages,
    settings::Settings::{Dims, Strings, Types},
};

use super::{
    buttonpage::PageButton,
    drawer::{Drawables, Rectangle, UIFont, UILabel},
    label::Label,
    layout::layout,
    Click, ClickableWidget, Location, Widget,
};
/*
On any page other than home, display:
    Home Button
    Battery SOC
On Home page
    Trailer Name
    Battery SOC

On select highlight the button
On rebuild:
    Grab the battery soc and display
*/
#[derive(Clone)]
pub struct Header {
    loc: Location,
    battery_soc: Label,
    home_button: Option<PageButton>,
    sender: Types::SDrawables,
}
impl Header {
    pub fn new(
        page: Option<Pages>,
        loc: Location,
        sender: Types::SDrawables,
        action_sender: Types::SEvents,
    ) -> Self {
        let l = layout(2, 1, loc.clone());
        let mut new_l = l[1].clone();
        new_l.point = new_l.point + Dims::BUTTON_OFFSET;
        let soc = Label::new(
            l[1].clone(),
            Some("%"),
            DataValue::BatteryInfo {
                f: BatteryInfo::Soc {
                    state: f32::default(),
                },
            },
            None,
            sender.clone(),
            action_sender.clone(),
            Some(UIFont::SMLabel),
        );
        let mut h = Header {
            loc,
            home_button: None,
            battery_soc: soc,
            sender: sender.clone(),
        };
        let layout = layout(2, 1, h.loc.clone());
        if let Some(p) = page {
            h.home_button = Some(PageButton::new(
                p,
                layout[0].clone(),
                h.sender.clone(),
                action_sender,
            ));
        }
        h
    }
}
impl Widget for Header {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.battery_soc.get_loc()
    }
    fn get_state(&self) -> super::drawer::RecState {
        self.battery_soc.get_state()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        self.battery_soc.get_sender()
    }
    fn draw_initial(&self) {
        // Draw the battery soc
        let r = Rectangle::new(self.loc.clone(), None, true);
        block_on(self.sender.send(Drawables::Rectangle { obj: r }));

        self.battery_soc.draw_initial();

        match &self.home_button {
            Some(b) => {
                info!("Drawing go Home button");
                b.draw_initial();
            }
            None => {
                info!("Writing Title");
                let offset = Point::new(3, 5);
                let layout = layout(2, 1, self.loc.clone());
                let l_new = layout[0].translate(offset).resize(-2);
                let l = UILabel::new(l_new, String::from(Strings::TITLE), UIFont::SMLabel, true);
                block_on(self.sender.send(Drawables::Label { obj: l }));
            }
        }
    }
    fn draw(&self, new_val: Option<DataValue>) {
        if let None = &new_val {
            self.draw_initial();
        }
        self.battery_soc.draw(new_val);
    }
}
impl ClickableWidget for Header {
    fn event_handler(&mut self, event: Click) {
        if let Some(b) = &mut self.home_button {
            b.event_handler(event);
        }
    }
}
