use defmt::info;
use embassy_futures::block_on;
use embedded_graphics::pixelcolor::Rgb666;
use heapless::Vec;

use crate::{
    components::convert_led,
    datastore::{DataValue, Deq},
    page::Pages,
    settings::Settings::{Colors, Types},
};

use super::{
    drawer::{Drawables, RecState, Rectangle},
    layout::layout,
    Click, ClickableWidget, Events, Location, Widget,
};
/*
Displays state of relays
    Watch for UI rebuild request for ID
    On draw grab the state of relays from pub/sub
*/
pub struct ButtonGroup {
    rows: u8,
    cols: u8,
    loc: Location,
    data: Vec<DataValue, 4>,
    page: Option<Pages>,
    state: RecState,
    sender: Types::SDrawables,
    action_sender: Types::SEvents,
}
impl ButtonGroup {
    pub fn new(
        rows: u8,
        cols: u8,
        loc: Location,
        page: Option<Pages>,
        data: Vec<DataValue, 4>,
        sender: Types::SDrawables,
        action_sender: Types::SEvents,
    ) -> Self {
        Self {
            rows,
            cols,
            loc,
            data,
            page,
            state: RecState::Clear,
            sender,
            action_sender,
        }
    }
    fn draw_single(&self, new_val: &DataValue, color: Option<Rgb666>) {
        let loc = layout(self.cols, self.rows, self.loc.clone());
        for i in 0..self.data.len() {
            if let DataValue::Relays { f } = &new_val {
                if self.data[i].deq(&new_val) {
                    let data_color = match f.try_get() {
                        Ok(state) => match state {
                            true => Colors::B_ON,
                            false => Colors::B_OFF,
                        },
                        Err(_) => panic!("Failed to get bool from Relay ui struct"),
                    };
                    let b_color = match color {
                        Some(rgb) => rgb,
                        None => data_color,
                    };
                    let r = Rectangle::new(loc[i].clone(), Some(b_color), true);
                    info!(
                        "Drawing buttongroup element for {:?} at {:?}",
                        new_val, loc[i]
                    );
                    block_on(self.sender.send(Drawables::Rectangle { obj: r }));
                }
            } else if let DataValue::RGBLed { f } = &new_val {
                if self.data[i].deq(&new_val) {
                    let data_color = match f.try_get() {
                        Ok(state) => state.get_color(),
                        Err(_) => panic!("Failed to get color from RGBMode"),
                    };
                    let b_color: Rgb666 = match color {
                        Some(rgb) => rgb,
                        None => convert_led(data_color.clone()),
                    };
                    let r = Rectangle::new(loc[i].clone(), Some(b_color), true);
                    info!(
                        "Drawing group for rgb led for {:?} at {:?} in color {:?}",
                        new_val,
                        loc[i],
                        f.try_get().expect("Unable to get RGBMode from datavalue")
                    );
                    block_on(self.sender.send(Drawables::Rectangle { obj: r }));
                }
            }
        }
    }
}

impl Widget for ButtonGroup {
    type Color = Rgb666;
    fn get_state(&self) -> RecState {
        self.state.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn draw_initial(&self) {
        info!("Drawing buttons in group");
        for i in 0..self.data.len() {
            self.draw_single(&self.data[i], Some(Colors::B_INACTIVE));
        }
        info!("Finished drawing buttons in group");
    }
    fn draw(&self, val: Option<DataValue>) {
        // If None then draw everything as in progress
        // Else draw just the recieved
        self.clear_outline();
        match val {
            Some(new_val) => self.draw_single(&new_val, None),
            None => {
                self.draw_initial();
            }
        }
        self.draw_outline();
    }
}
impl ClickableWidget for ButtonGroup {
    fn event_handler(&mut self, event: Click) {
        self.clear_outline();
        info!("Buttongroup Got a {:?}", event);
        match event {
            Click::None => {
                self.state = RecState::Clear;
            }
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up | Click::Long => {
                if let Some(p) = self.page.clone() {
                    block_on(self.action_sender.send(Events::Page { page: p }));
                }
            }
        }
        self.draw_outline();
    }
}
