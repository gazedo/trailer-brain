use super::{
    drawer::{Drawables, RecState, Rectangle},
    Click, ClickableWidget, Events, Location, Widget,
};
use crate::{
    datastore::{BatteryInfo, DataValue, Deq, FuelInfo},
    page::Pages,
    settings::Settings::{Colors, Types},
};
use defmt::info;
use embassy_futures::{block_on, join::join};
use embedded_graphics::{
    pixelcolor::Rgb666,
    prelude::{Point, Size},
};
/*
Displays state of Fuel
    Watch for UI rebuild request for ID
    On draw grab the state of fuel from pub/sub
*/
pub enum Orientation {
    Horizontal,
    Vertical,
}
#[derive(Clone, Debug)]
pub enum Fuel {
    Water,
    Battery,
}
pub struct FuelGauge {
    amount: DataValue,
    page: Option<Pages>,
    kind: Fuel,
    orientation: Orientation,
    loc: Location,
    state: RecState,
    sender: Types::SDrawables,
    action_sender: Types::SEvents,
}

impl FuelGauge {
    pub fn new(
        page: Option<Pages>,
        kind: Fuel,
        loc: Location,
        orientation: Orientation,
        sender: Types::SDrawables,
        action_sender: Types::SEvents,
    ) -> Self {
        let amount = match kind {
            Fuel::Water => DataValue::FuelInfo {
                f: FuelInfo::Water { state: 0.0 },
            },
            Fuel::Battery => DataValue::BatteryInfo {
                f: BatteryInfo::Soc { state: 0.0 },
            },
        };

        FuelGauge {
            amount,
            page,
            loc,
            kind,
            orientation,
            state: RecState::Clear,
            sender,
            action_sender,
        }
    }
    pub fn selectable(&self) -> bool {
        if let Some(_) = self.page {
            true
        } else {
            false
        }
    }
}
// Draw then entire size with the colored part equal to value and the rest colored to match background
impl Widget for FuelGauge {
    type Color = Rgb666;
    fn get_loc(&self) -> Location {
        self.loc.clone()
    }
    fn get_state(&self) -> RecState {
        self.state.clone()
    }
    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }
    fn draw(&self, new_val: Option<DataValue>) {
        let amount = match new_val {
            Some(d) => {
                if self.amount.deq(&d) {
                    if let DataValue::FuelInfo { f } = d {
                        match f.try_get() {
                            Ok(val) => val.clone(),
                            Err(_) => panic!("Failed to get f32 from FuelInfo"),
                        }
                    } else if let DataValue::BatteryInfo { f } = d {
                        if let BatteryInfo::Soc { state } = f {
                            state
                        } else {
                            // Not the soc so return and leave the screen alone
                            return;
                        }
                    } else {
                        panic!("Invalid datavalue in fuelgauge");
                    }
                } else {
                    return;
                }
            }
            None => 0.0,
        };
        let fill_color = if let Fuel::Water = self.kind {
            Colors::FG_WATER
        } else {
            Colors::FG_BATTERY
        };
        let p = self.loc.point;
        let (new_p, new_s) = match self.orientation {
            Orientation::Horizontal => {
                let new_p = Point::new(p.x, p.y);
                let new_s = Size::new(
                    (amount / 100.00 * self.loc.size.width as f32) as u32,
                    self.loc.size.height,
                );
                (new_p, new_s)
            }
            Orientation::Vertical => {
                let new_p = Point::new(
                    p.x,
                    p.y + self.loc.size.height as i32
                        - (self.loc.size.height as f32 * amount / 100.00) as i32,
                );
                let new_s = Size::new(
                    self.loc.size.width,
                    (amount * self.loc.size.height as f32) as u32,
                );
                (new_p, new_s)
            }
        };
        let outline = Rectangle::new(self.loc.clone(), Some(Colors::BACKGROUND), true);
        info!(
            "Drawing fuelgauge fill with value {:?} loc {:?} with initial loc {:?} and orientation {:?}",
            amount,
            Location {
                point: new_p,
                size: new_s
            },
            self.loc,
            match self.orientation {
                Orientation::Horizontal => 0,
                Orientation::Vertical => 1,
            }
        );
        let fill = Rectangle::new(
            Location {
                point: new_p,
                size: new_s,
            },
            Some(fill_color),
            false,
        );
        let f1 = self.sender.send(Drawables::Rectangle { obj: outline });
        let f2 = self.sender.send(Drawables::Rectangle { obj: fill });

        block_on(join(f1, f2));
        // block_on(f2);
        self.draw_outline();
    }
    fn draw_initial(&self) {
        self.draw(None);
    }
}
impl ClickableWidget for FuelGauge {
    fn event_handler(&mut self, event: Click) {
        self.clear_outline();
        match event {
            Click::None => {
                self.state = RecState::Clear;
            }
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up | Click::Long => {
                if let Some(data) = &self.page {
                    block_on(self.action_sender.send(Events::Page { page: data.clone() }));
                }
            }
        }
        self.draw_outline();
    }
}
