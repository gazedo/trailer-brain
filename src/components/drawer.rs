use defmt::{info, Format};
use embedded_graphics::{
    mono_font::MonoTextStyleBuilder,
    pixelcolor::Rgb666,
    prelude::Point,
    primitives::{self, Primitive, PrimitiveStyle, StrokeAlignment},
    text::{Alignment, Text},
    Drawable,
};
use heapless::String;

use crate::{
    components::Location,
    settings::Settings::{Colors, Constants, Dims, Fonts},
};

pub enum Drawables {
    Rectangle { obj: Rectangle },
    Outline { obj: Outline },
    Label { obj: UILabel },
}

#[derive(Debug)]
pub struct Rectangle {
    loc: Location,
    fill_color: Option<Rgb666>,
    outline_enabled: bool,
}
#[derive(Debug)]
pub struct Outline {
    loc: Location,
    hover: RecState,
}
#[derive(Debug)]
pub struct UILabel {
    loc: Location,
    text: String<{ Constants::L_LABEL }>,
    font: UIFont,
    blank: bool,
}

#[derive(Debug, Clone, Format)]
pub enum RecState {
    Hover,
    Clear,
}

impl Rectangle {
    pub fn new(loc: Location, fill_color: Option<Rgb666>, outline_enabled: bool) -> Self {
        Rectangle {
            loc,
            fill_color,
            outline_enabled,
        }
    }
}

impl Drawable for Rectangle {
    type Color = Rgb666;

    type Output = ();

    fn draw<D>(&self, target: &mut D) -> Result<Self::Output, D::Error>
    where
        D: embedded_graphics::prelude::DrawTarget<Color = Self::Color>,
    {
        let mut raw_style = PrimitiveStyle::<Rgb666>::default();
        if self.outline_enabled {
            raw_style.stroke_alignment = StrokeAlignment::Inside;
            raw_style.stroke_color = Some(Colors::B_BORDER);
            raw_style.stroke_width = Dims::OUTLINE;
        }
        raw_style.fill_color = Some(match self.fill_color {
            Some(fc) => fc,
            None => Colors::BACKGROUND,
        });
        primitives::Rectangle::new(self.loc.get_point(), self.loc.get_size())
            .into_styled(raw_style)
            .draw(target)?;
        info!(
            "Sent rectangle to be drawn at {:?} with borders {}",
            self.loc, self.outline_enabled
        );
        Ok(())
    }
}
impl Outline {
    pub fn new(loc: Location, hover: RecState) -> Self {
        Self { loc, hover }
    }
}
impl Drawable for Outline {
    type Color = Rgb666;

    type Output = ();
    fn draw<D>(&self, target: &mut D) -> Result<Self::Output, D::Error>
    where
        D: embedded_graphics::prelude::DrawTarget<Color = Self::Color>,
    {
        let mut raw_style = PrimitiveStyle::<Rgb666>::default();
        match self.hover {
            RecState::Hover => {
                raw_style.stroke_alignment = StrokeAlignment::Outside;
                raw_style.stroke_width = Dims::SELECTED;
                raw_style.stroke_color = Some(Colors::SELECTED);
            }
            RecState::Clear => {
                raw_style.stroke_alignment = StrokeAlignment::Outside;
                raw_style.stroke_width = Dims::SELECTED;
                raw_style.stroke_color = Some(Colors::OUTLINE);
            }
        };
        raw_style.fill_color = None;
        let new_loc = self
            .loc
            .translate(Point::new(
                Dims::OUTLINE_PADDING * -1,
                Dims::OUTLINE_PADDING * -1,
            ))
            .resize(1);
        primitives::Rectangle::new(new_loc.get_point(), new_loc.get_size())
            .into_styled(raw_style)
            .draw(target)?;
        info!(
            "Sent Outline to be drawn at {:?} with hover being {}",
            new_loc, self.hover
        );
        Ok(())
    }
}
impl UILabel {
    pub fn new(
        loc: Location,
        text: String<{ Constants::L_LABEL }>,
        font: UIFont,
        blank: bool,
    ) -> Self {
        UILabel {
            loc,
            text,
            font,
            blank,
        }
    }
}

#[derive(Clone, Debug)]
pub enum UIFont {
    PageButton,
    SMLabel,
    LLabel,
}
impl Drawable for UILabel {
    type Color = Rgb666;

    type Output = ();

    fn draw<D>(&self, target: &mut D) -> Result<Self::Output, D::Error>
    where
        D: embedded_graphics::prelude::DrawTarget<Color = Self::Color>,
    {
        if self.blank {
            let mut raw_style = PrimitiveStyle::<Rgb666>::default();
            raw_style.fill_color = Some(Colors::BACKGROUND);
            primitives::Rectangle::new(self.loc.get_point(), self.loc.get_size())
                .into_styled(raw_style)
                .draw(target)?;
        }
        // let mut border_style = PrimitiveStyle::<Rgb666>::default();
        // border_style.fill_color = None;
        // border_style.stroke_color = Some(Colors::B_BORDER);
        // border_style.stroke_alignment = StrokeAlignment::Inside;
        // border_style.stroke_width = Dims::OUTLINE;
        // primitives::Rectangle::new(self.loc.get_point(), self.loc.get_size())
        //     .into_styled(border_style)
        //     .draw(target)?;

        let font = match self.font {
            UIFont::PageButton => Fonts::PAGEB_FONT,
            UIFont::SMLabel => Fonts::SMALL_LABEL,
            UIFont::LLabel => Fonts::LARGE_LABEL,
        };
        let style = MonoTextStyleBuilder::new()
            .text_color(Colors::B_TEXT)
            .font(&font)
            .build();

        let t = Text::with_alignment(
            self.text.as_str(),
            self.loc.get_center(),
            style,
            Alignment::Center,
        );
        t.draw(target)?;
        info!(
            "Sent text to be drawn at center of {:?} with text [{}] and blank is {}",
            self.loc,
            self.text.as_str(),
            self.blank
        );
        Ok(())
    }
}
