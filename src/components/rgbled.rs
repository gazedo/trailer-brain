use defmt::{info, Format};
use embassy_futures::{block_on, join::join};
use embedded_graphics::pixelcolor::Rgb666;
use heapless::String;
use smart_leds::RGB8;

use crate::{
    components::{
        convert_led,
        drawer::{Drawables, Rectangle},
    },
    datastore::{DataValue, Deq, Msg, RGBInfo},
    settings::Settings::{Colors, Dims, Strings, Types},
};

use super::{
    drawer::{RecState, UIFont, UILabel},
    Click, ClickableWidget, Location, Widget,
};

// Easy on/off
// Options of:
//     toggle
// Mode buttons
//     High WHITE
//     multi
//     night mode
#[derive(Clone, Debug, Format, PartialEq)]
pub enum RGBMode {
    HWhite,
    Night,
}
impl RGBMode {
    pub fn get_color(&self) -> RGB8 {
        match self {
            Self::HWhite => Colors::H_WHITE,
            Self::Night => Colors::NIGHT_MODE,
        }
    }
    pub fn advance_mode(&self) -> RGBMode {
        match self {
            Self::HWhite => RGBMode::Night,
            Self::Night => RGBMode::HWhite,
        }
    }
}
pub struct LEDLabels<'a> {
    pub main_label: &'a str,
    pub desc_label: Option<&'a str>,
}
impl LEDLabels<'_> {
    pub async fn loc_draw(&self, l: Location, sender: &Types::SDrawables) {
        let s1 = sender.send(Drawables::Label {
            obj: UILabel::new(
                l.translate(Dims::BUTTON_OFFSET),
                String::from(self.main_label),
                UIFont::LLabel,
                false,
            ),
        });
        if let Some(l_desc) = self.desc_label {
            let s2 = sender.send(Drawables::Label {
                obj: UILabel::new(
                    l.translate(Dims::DESC_OFFSET),
                    String::from(l_desc),
                    UIFont::SMLabel,
                    false,
                ),
            });
            block_on(join(s1, s2));
        } else {
            block_on(s1);
        }
    }
}

pub struct RGBLed {
    data: DataValue,
    state: RecState,
    loc: Location,
    sender: Types::SDrawables,
    hw_sender: Types::SHw,
}

impl RGBLed {
    pub fn new(
        data: DataValue,
        loc: Location,
        sender: Types::SDrawables,
        action_s: Types::SHw,
    ) -> Self {
        RGBLed {
            data,
            state: RecState::Clear,
            loc,
            sender,
            hw_sender: action_s,
        }
    }
}

impl Widget for RGBLed {
    type Color = Rgb666;

    fn get_loc(&self) -> Location {
        self.loc.clone()
    }

    fn get_state(&self) -> RecState {
        self.state.clone()
    }

    fn get_sender(&self) -> &Types::SDrawables {
        &self.sender
    }

    // Gets a message which details current state and current mode
    // On None, draw with inactive colors
    // On Datavalue
    fn draw(&self, val: Option<DataValue>) {
        let new_val = match val {
            Some(v) => v,
            None => self.data.clone(),
        };
        if self.data.deq(&new_val) {
            info!(
                "Drawing RGBLed Button with new_val {:?} loc {:?}",
                &new_val, self.loc
            );
            let fill_color = if let DataValue::RGBLed { f } = &new_val {
                match f.try_get() {
                    Err(e) => panic!("Got error {:?}", e),
                    Ok(b) => {
                        let l = b.get_color();
                        convert_led(l)
                    }
                }
            } else {
                defmt::panic!("Got an invalid datavalue field in a button {:?}", new_val);
            };

            let r = Rectangle::new(self.loc.clone(), Some(fill_color), true);
            block_on(self.sender.send(Drawables::Rectangle { obj: r }));
            let bl = match &new_val {
                DataValue::RGBLed { f } => match f {
                    RGBInfo::Led0 { mode: _ } => LEDLabels {
                        main_label: Strings::LED0,
                        desc_label: None,
                    },
                    RGBInfo::Led1 { mode: _ } => LEDLabels {
                        main_label: Strings::LED1,
                        desc_label: None,
                    },
                    // RGBInfo::Led2 { mode: _ } => LEDLabels {
                    //     main_label: Strings::LED2,
                    //     desc_label: None,
                    // },
                    // RGBInfo::Led3 { mode: _ } => LEDLabels {
                    //     main_label: Strings::LED3,
                    //     desc_label: None,
                    // },
                },
                DataValue::BatteryInfo { f: _ }
                | DataValue::FuelInfo { f: _ }
                | DataValue::Relays { f: _ }
                | DataValue::IMUInfo { f: _ } => {
                    panic!("Invalid match arm for button");
                }
            };
            block_on(bl.loc_draw(self.loc.clone(), &self.sender));
        }
    }

    fn draw_initial(&self) {
        self.draw(None);
    }
}
impl ClickableWidget for RGBLed {
    // click sends a toggle message which turns the led mode on/off
    // Long click sends a state change request which changes the mode which is reflected in the main button through a gui update
    fn event_handler(&mut self, event: Click) {
        match event {
            Click::Hover => {
                self.state = RecState::Hover;
            }
            Click::Up => {
                block_on(self.hw_sender.send(Msg::Toggle {
                    field: self.data.clone(),
                }));
            }
            Click::None => {
                self.state = RecState::Clear;
                self.clear_outline();
            }
            Click::Long => {
                block_on(self.hw_sender.send(Msg::State {
                    state: self.data.clone(),
                }));
            }
        }
        self.draw_outline();
    }
}
