pub mod button;
pub mod buttongroup;
pub mod buttonpage;
pub mod drawer;
pub mod fuelgauge;
pub mod header;
pub mod imu_canvas;
pub mod label;
pub mod layout;
pub mod rgbled;

use core::fmt::Debug;

use crate::{
    datastore::DataValue,
    page::{Pages, WheelAction},
    settings::Settings::{Colors, Types},
};

use defmt::Format;
use embassy_futures::block_on;
use embedded_graphics::{
    pixelcolor::Rgb666,
    prelude::{Point, Size},
};
use smart_leds::RGB8;

use self::{
    button::Button,
    buttongroup::ButtonGroup,
    drawer::{Drawables, Outline, RecState, Rectangle},
    fuelgauge::FuelGauge,
    header::Header,
    imu_canvas::ImuCanvas,
    label::Label,
    rgbled::RGBLed,
};

// Change to populate updated queue with components that were updated so only those are drawn
// Change interface to populate a default drawing function that works through the queue for that component
// Or should the drawing queue be a channel that pushes to a thread?

pub fn convert_led(color: RGB8) -> Rgb666 {
    Rgb666::new(color.r, color.g, color.b)
}

pub trait Widget {
    type Color;
    fn get_loc(&self) -> Location;
    fn get_state(&self) -> RecState;
    fn get_sender(&self) -> &Types::SDrawables;
    fn draw(&self, new_val: Option<DataValue>);
    fn draw_initial(&self);
    fn clear(&self) {
        let clear_obj = Rectangle::new(self.get_loc(), Some(Colors::BACKGROUND), false);
        let s = self
            .get_sender()
            .send(Drawables::Rectangle { obj: clear_obj });
        block_on(s);
    }
    fn clear_outline(&self) {
        let outline_clear = Outline::new(self.get_loc(), RecState::Clear);
        let s = self
            .get_sender()
            .send(Drawables::Outline { obj: outline_clear });
        block_on(s);
    }
    fn draw_outline(&self) {
        let outline_clear = Outline::new(self.get_loc(), self.get_state());
        let s = self
            .get_sender()
            .send(Drawables::Outline { obj: outline_clear });
        block_on(s);
    }
}
// Handles drawing the appropriate style to the button or clicks it. 3 possible states, None, Hover, click
pub trait ClickableWidget: Widget {
    fn event_handler(&mut self, event: Click);
}

#[derive(Clone, Debug)]
pub struct Location {
    point: Point,
    size: Size,
}
impl defmt::Format for Location {
    fn format(&self, fmt: defmt::Formatter) {
        let x = self.point.x;
        let y = self.point.y;
        let sx = self.size.width;
        let sy = self.size.height;
        defmt::write!(
            fmt,
            "Location {{ Point {:?}, {:?}, Size {:?},{:?} }}",
            x,
            y,
            sx,
            sy
        );
    }
}
impl Location {
    pub const fn new(point: Point, size: Size) -> Location {
        Location { point, size }
    }
    pub fn get_point(&self) -> Point {
        self.point
    }
    pub fn get_size(&self) -> Size {
        self.size
    }
    pub fn get_center(&self) -> Point {
        let x_offset = self.size.width as f32 / 2.0;
        let y_offset = self.size.height as f32 / 2.0;
        let offset = Point::new(x_offset as i32, y_offset as i32);
        self.point + offset
    }
    pub fn get_bottom_right(&self) -> Point {
        self.point + Point::new(self.size.width as i32, self.size.height as i32)
    }
    pub fn contains(&self, p: Point) -> bool {
        if p > self.point && p < self.get_bottom_right() {
            true
        } else {
            false
        }
    }
    pub fn translate(&self, p: Point) -> Location {
        let new_p = self.point + p;
        // let new_w = match (self.size.width as i32 - p.x).try_into() {
        //     Ok(val) => val,
        //     Err(_) => {
        //         warn!("Failed to convert size in translate setting h to 0");
        //         0
        //     }
        // };
        // let new_h = match (self.size.height as i32 - p.y).try_into() {
        //     Ok(val) => val,
        //     Err(_) => {
        //         warn!("Failed to convert size in translate setting h to 0");
        //         0
        //     }
        // };
        // let new_s = Size::new(new_w, new_h);
        Location {
            point: new_p,
            size: self.size,
        }
    }
    pub fn resize(&self, s: i32) -> Location {
        // Resizes the size value by adding the given value to each dimension
        Location::new(
            self.point,
            Size::new(
                u32::try_from(self.size.width as i32 + (2 * s))
                    .expect("Got a negative value when resizing"),
                u32::try_from(self.size.height as i32 + (2 * s))
                    .expect("Got a negative value when resizing"),
            ),
        )
    }
}

#[derive(Clone, Debug, Format)]
pub enum Click {
    None,
    Hover,
    Up,
    Long,
}
#[derive(Clone, Debug, defmt::Format)]
pub enum Events {
    ReDraw { id: Option<DataValue> },
    Wheel { action: WheelAction },
    Page { page: Pages },
}

pub enum Component {
    Button { b: Button },
    ButtonGroup { b: ButtonGroup },
    FuelGauge { b: FuelGauge },
    ImuCanvas { b: ImuCanvas },
    Label { b: Label },
    Header { b: Header },
    RGBLed { b: RGBLed },
}
impl Debug for Component {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Button { b: _ } => f.write_str("Button"),
            Self::ButtonGroup { b: _ } => f.write_str("ButtonGroup"),
            Self::FuelGauge { b: _ } => f.write_str("FuelGroup"),
            Self::ImuCanvas { b: _ } => f.write_str("ImuCanvas"),
            Self::Label { b: _ } => f.write_str("Label"),
            Self::Header { b: _ } => f.write_str("Header"),
            Self::RGBLed { b: _ } => f.write_str("RGBLed"),
        }
        // match self {
        //     Self::Button { b } => f.debug_struct("Button").field("b", b).finish(),
        //     Self::ButtonGroup { b } => f.debug_struct("ButtonGroup").field("b", b).finish(),
        //     Self::FuelGauge { b } => f.debug_struct("FuelGauge").field("b", b).finish(),
        //     Self::ImuCanvas { b } => f.debug_struct("ImuCanvas").field("b", b).finish(),
        //     Self::Label { b } => f.debug_struct("Label").field("b", b).finish(),
        //     Self::Header { b } => f.debug_struct("Header").field("b", b).finish(),
        // }
    }
}
impl Component {
    pub async fn event_handler(&mut self, event: Click) {
        match self {
            Component::Button { b } => {
                b.event_handler(event);
            }
            Component::ButtonGroup { b } => {
                b.event_handler(event);
            }
            Component::FuelGauge { b } => {
                b.event_handler(event);
            }
            Component::ImuCanvas { b } => {
                b.event_handler(event);
            }
            Component::Label { b } => {
                b.event_handler(event);
            }
            Component::Header { b } => {
                b.event_handler(event);
            }
            Component::RGBLed { b } => {
                b.event_handler(event);
            }
        };
    }
    // pub fn draw_initial(&self) {
    //     match self {
    //         Component::Button { b } => {
    //             b.draw_initial();
    //         }
    //         Component::ButtonGroup { b } => {
    //             b.draw_initial();
    //         }
    //         Component::FuelGauge { b } => {
    //             b.draw_initial();
    //         }
    //         Component::ImuCanvas { b } => {
    //             b.draw_initial();
    //         }
    //         Component::Label { b } => {
    //             b.draw_initial();
    //         }
    //         Component::Header { b } => {
    //             b.draw_initial();
    //         }
    //     };
    // }
    pub fn draw(&self, id: Option<DataValue>) {
        match self {
            Component::Button { b } => {
                b.draw(id);
            }
            Component::ButtonGroup { b } => {
                b.draw(id);
            }
            Component::FuelGauge { b } => {
                b.draw(id);
            }
            Component::ImuCanvas { b } => {
                b.draw(id);
            }
            Component::Label { b } => {
                b.draw(id);
            }
            Component::Header { b } => {
                b.draw(id);
            }
            Component::RGBLed { b } => {
                b.draw(id);
            }
        };
    }
}
