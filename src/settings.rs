// #![allow(non_snake_case, unused)]
#![allow(non_snake_case)]

pub mod Settings {
    // Sizes for heapless structs
    pub mod Constants {
        use embassy_time::Duration;

        // Range for IMU point to turn to P_GOOD
        pub const IMU_TARGET: f32 = 0.5;
        // Size of string used for labels
        pub const L_LABEL: usize = 80;
        // Size of string used for labels units
        pub const L_UNIT: usize = 2;
        // Constants that dictate how long the channels
        pub const Q_EVENT: usize = 5;
        pub const Q_CANVAS: usize = 30;
        // Constants that dictate screen objects
        pub const N_PAGE_ELEMENTS: usize = 15;
        pub const N_CELL_ELEMENTS: usize = 10;
        // Constants for channels and pubsubs
        pub const N_BYTES: usize = 12;
        pub const N_SIZE: usize = 4;
        pub const N_SUBS: usize = 5;
        pub const N_PUBS: usize = 4;
        pub const N_PAGES: usize = 6;
        // Length of LED strips
        pub const LED0_LEN: usize = 280;
        pub const LED1_LEN: usize = 165;
        // pub const LED2_LEN: usize = 165;
        // pub const LED3_LEN: usize = 165;
        // Times for click
        pub const LONG_PRESS: Duration = Duration::from_millis(1000);
        pub const DEBOUNCE: Duration = Duration::from_millis(300);
    }
    pub mod Colors {
        use embedded_graphics::{pixelcolor::Rgb666, prelude::*};
        use smart_leds::RGB8;
        // General Background
        pub const BACKGROUND: Rgb666 = Rgb666::BLACK;
        // Not selected outline
        pub const OUTLINE: Rgb666 = BACKGROUND;
        // Selected outline
        pub const SELECTED: Rgb666 = Rgb666::BLUE;
        // Inactive button before it gets info
        pub const B_INACTIVE: Rgb666 = BACKGROUND;
        // Button Outline
        pub const B_BORDER: Rgb666 = Rgb666::new(0, 96, 255);
        // Button text color
        pub const B_TEXT: Rgb666 = Rgb666::new(0, 96, 255);
        // Button when state is off
        pub const B_OFF: Rgb666 = Rgb666::new(0, 160, 240);
        // Button when state is on
        pub const B_ON: Rgb666 = Rgb666::GREEN;
        // IMU Canvas background
        pub const IMU_CANVAS: Rgb666 = Rgb666::WHITE;
        // FG color for battery soc
        pub const FG_BATTERY: Rgb666 = Rgb666::YELLOW;
        // FG color for water amount
        pub const FG_WATER: Rgb666 = Rgb666::BLUE;
        // IMU Canvas point color when in range
        pub const P_GOOD: Rgb666 = Rgb666::GREEN;
        // IMU Canvas point color when out of range
        pub const P_BAD: Rgb666 = Rgb666::RED;
        // LED Colors for each mode
        pub const H_WHITE: RGB8 = RGB8::new(255, 255, 255);
        pub const NIGHT_MODE: RGB8 = RGB8::new(150, 0, 0);
        pub const LED_OFF: RGB8 = RGB8::new(0, 0, 0);
    }
    pub mod Dims {
        use crate::components::Location;
        use embedded_graphics::prelude::{Point, Size};
        // Width of outline when not selected
        pub const OUTLINE: u32 = 1;
        // Width of outline when selected
        pub const SELECTED: u32 = 3;
        // Space between object and selected outline
        pub const OUTLINE_PADDING: i32 = 1;
        // Space in between graphical objects
        pub const PADDING: f32 = 5.0;
        // Offset for button text
        pub const BUTTON_OFFSET: Point = Point::new(0, -5);
        // Offset for button description
        pub const DESC_OFFSET: Point = Point::new(0, 20);
        // Offset for f32 values in Location
        pub const F32_OFFSET: Point = Point::new(0, 3);
        // Size of point for IMU canvas
        pub const IMU_PNT_SIZE: Size = Size::new(7, 7);
        // Size of header
        pub const HEADER: Location = Location::new(Point::new(0, 0), Size::new(240, 20));
        // Size of main pane
        pub const MAIN_PANE: Location = Location::new(Point::new(0, 20), Size::new(240, 300));
        // pub const SCREEN_SIZE: Rectangle = Rectangle::new(Point::new(0, 0), Size::new(320, 240));
    }

    pub mod Strings {
        pub static BATTERY: &str = "Battery";
        pub static TITLE: &str = "Trailer Brain";
        pub static HOME: &str = "Home";
        pub static BUTTON: &str = "Buttons";
        pub static IMU: &str = "Imu";
        pub static PREP_AREA: &str = "Prep Area";
        pub static STOVE_AREA: &str = "Stove Area";
        pub static FRIDGE: &str = "Fridge";
        pub static WATER: &str = "Water Pump";
        pub static RGB_LED: &str = "RGB Led";
        pub static ZERO: &str = "Zero";
        pub static LED0: &str = "Awning";
        pub static LED1: &str = "Interior";
        // pub static LED2: &str = "Not Installed";
        // pub static LED3: &str = "Not Installed";
    }
    pub mod Fonts {
        use embedded_graphics::mono_font::{
            ascii::{FONT_10X20, FONT_5X8, FONT_8X13},
            MonoFont,
        };
        // Used for small font on header button
        pub const PAGEB_FONT: MonoFont = FONT_5X8;
        // Description font for buttons
        pub const SMALL_LABEL: MonoFont = FONT_8X13;
        // Font for button main label and info labels
        pub const LARGE_LABEL: MonoFont = FONT_10X20;
    }
    pub mod Types {
        use embassy_sync::{
            blocking_mutex::raw::NoopRawMutex,
            channel::{Receiver, Sender},
            pubsub::{Publisher, Subscriber},
        };

        use crate::{
            components::{drawer::Drawables, Events},
            datastore::{DataValue, Msg},
            page::Pages,
        };

        use super::Constants;

        // pub type RHw<T> = Receiver<'static, NoopRawMutex, Msg<T>, { Constants::N_SIZE }>;
        // pub type SHw<T> = Sender<'static, NoopRawMutex, Msg<T>, { Constants::N_SIZE }>;
        pub type RHw = Receiver<'static, NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>;
        pub type SHw = Sender<'static, NoopRawMutex, Msg<DataValue>, { Constants::N_SIZE }>;
        pub type REvents = Receiver<'static, NoopRawMutex, Events, { Constants::Q_EVENT }>;
        pub type SEvents = Sender<'static, NoopRawMutex, Events, { Constants::Q_EVENT }>;
        pub type RWake = Receiver<'static, NoopRawMutex, bool, { Constants::N_SIZE }>;
        pub type SWake = Sender<'static, NoopRawMutex, bool, { Constants::N_SIZE }>;
        pub type RPages = Subscriber<
            'static,
            NoopRawMutex,
            Pages,
            { Constants::N_SIZE },
            { Constants::N_SUBS },
            { Constants::N_PUBS },
        >;
        pub type SPages = Publisher<
            'static,
            NoopRawMutex,
            Pages,
            { Constants::N_SIZE },
            { Constants::N_SUBS },
            { Constants::N_PUBS },
        >;
        pub type SDrawables = Sender<'static, NoopRawMutex, Drawables, { Constants::Q_CANVAS }>;
    }
}
