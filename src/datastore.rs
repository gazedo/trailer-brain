// For values like water level or battery soc need to be able to be written to by external threads
// For gpio pins, need to allow toggling of that gpio from the button
// For buttons that change pages need to write to channel

// Need to allow the object to send object ID's to UI that need to be updated when object is updated
// Can't assume single access so needs access restriction like mutex?

/*
V1
GPIO <bool>
    write will allow settings state or toggling on none
    read will retrieve current state
F32 for battery soc, water, propane, angles
    write is used by thread to update value
    read is used by UI to update

all write methods are required to send UI ID to UI event channel for UI update trigger
*/

/*
V2
Data is the package to identify and hold all values
CMD is used to sync between Data structs

IE battery thread gets an update for battery soc
- Sends a pub command that holds a Update<Data> in a cmd struct
- Sends a UI update request for the type of Data using ?

If the UI is rebuilding and sends a Request command then the battery thread will reply with the Update data cmd

IE a button is pushed
- UI sends a cmd to toggle the gpio
- Gpio responds with an update

Treat hardware threads like servers
UI is a async client that gets notified for an update via multi wake registration? If shown get update?
Server pays attention to requests
Clients pay attention to updates
Maybe model on a http server?
*/

use core::fmt::{self, Debug, Display};

use defmt::Format;

use crate::components::rgbled::RGBMode;

#[derive(Debug)]
pub enum DataError {
    NotCorrectDV,
}
pub trait Deq {
    fn deq(&self, rhs: &Self) -> bool;
}
// Datavalue that stores the value and identifies it
#[derive(Debug, Clone, PartialEq, Format)]
pub enum DataValue {
    Relays { f: Relays },
    FuelInfo { f: FuelInfo },
    BatteryInfo { f: BatteryInfo },
    IMUInfo { f: IMUInfo },
    RGBLed { f: RGBInfo },
}
impl Deq for DataValue {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (DataValue::Relays { f: f1 }, DataValue::Relays { f: f2 }) => f1.deq(f2),
            (DataValue::FuelInfo { f: f1 }, DataValue::FuelInfo { f: f2 }) => f1.deq(f2),
            (DataValue::BatteryInfo { f: f1 }, DataValue::BatteryInfo { f: f2 }) => f1.deq(f2),
            (DataValue::IMUInfo { f: f1 }, DataValue::IMUInfo { f: f2 }) => f1.deq(f2),
            _ => false,
        }
    }
}
impl fmt::Display for DataValue {
    fn fmt(&self, fmt: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match &self {
            DataValue::Relays { f } => fmt
                .write_fmt(format_args!("{}", f))
                .expect("Failed to format Datavalue::Relays"),
            DataValue::FuelInfo { f } => fmt
                .write_fmt(format_args!("{}", f))
                .expect("Failed to format Datavalue::FuelInfo"),
            DataValue::BatteryInfo { f } => fmt
                .write_fmt(format_args!("{}", f))
                .expect("Failed to format Datavalue::BatteryInfo"),
            DataValue::IMUInfo { f } => fmt
                .write_fmt(format_args!("{}", f))
                .expect("Failed to format Datavalue::IMUInfo"),
            DataValue::RGBLed { f } => fmt
                .write_fmt(format_args!("{:?}", f))
                .expect("Failed to format Datavalue::RGBInfo"),
        };
        Ok(())
    }
}
// impl Format for DataValue {
//     fn format(&self, fmt: Formatter) {
//         match self {
//             DataValue::Relays { f } => write!(
//                 fmt,
//                 "{:?}",
//                 f.try_get().expect("Unable to get Relay debug string")
//             ),
//             DataValue::FuelInfo { f } => write!(
//                 fmt,
//                 "{:?}",
//                 f.try_get().expect("Unable to get FuelInfo debug string")
//             ),
//             DataValue::BatteryInfo { f } => write!(
//                 fmt,
//                 "{:?}",
//                 f.try_get()
//                     .expect("Unable to get Battery info Debug string")
//             ),
//             DataValue::IMUInfo { f } => write!(
//                 fmt,
//                 "{:?}",
//                 f.try_get().expect("Unable to get debug IMUInfo")
//             ),
//         }
//     }
// }

// Stores relay states
#[derive(Debug, Clone, PartialEq, Format)]
pub enum Relays {
    Stove { state: bool },
    Prep { state: bool },
    Fridge { state: bool },
    Water { state: bool },
}
impl Relays {
    pub fn try_get(&self) -> Result<&bool, DataError> {
        Ok(match self {
            Relays::Stove { state } => state,
            Relays::Prep { state } => state,
            Relays::Fridge { state } => state,
            Relays::Water { state } => state,
        })
    }
}
impl Display for Relays {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Relays::Stove { state } => f.write_fmt(format_args!("{}", state)),
            Relays::Prep { state } => f.write_fmt(format_args!("{}", state)),
            Relays::Fridge { state } => f.write_fmt(format_args!("{}", state)),
            Relays::Water { state } => f.write_fmt(format_args!("{}", state)),
        }
    }
}
impl Deq for Relays {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (Relays::Stove { .. }, Relays::Stove { .. }) => true,
            (Relays::Prep { .. }, Relays::Prep { .. }) => true,
            (Relays::Fridge { .. }, Relays::Fridge { .. }) => true,
            (Relays::Water { .. }, Relays::Water { .. }) => true,
            _ => false,
        }
    }
}

// Stores RGBLed info
// Msg takes care of toggle or state change
#[derive(Debug, Clone, PartialEq, Format)]
pub enum RGBInfo {
    Led0 { mode: RGBMode },
    Led1 { mode: RGBMode },
    // Led2 { mode: RGBMode },
    // Led3 { mode: RGBMode },
}
impl RGBInfo {
    pub fn try_get(&self) -> Result<&RGBMode, DataError> {
        Ok(match self {
            Self::Led0 { mode } => mode,
            Self::Led1 { mode } => mode,
            // Self::Led2 { mode } => mode,
            // Self::Led3 { mode } => mode,
        })
    }
}
impl Display for RGBInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Led0 { mode } => f.write_fmt(format_args!("{:?}", mode)),
            Self::Led1 { mode } => f.write_fmt(format_args!("{:?}", mode)),
            // Self::Led2 { mode } => f.write_fmt(format_args!("{:?}", mode)),
            // Self::Led3 { mode } => f.write_fmt(format_args!("{:?}", mode)),
        }
    }
}
impl Deq for RGBInfo {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (Self::Led0 { .. }, Self::Led0 { .. }) => true,
            (Self::Led1 { .. }, Self::Led1 { .. }) => true,
            // (Self::Led2 { .. }, Self::Led2 { .. }) => true,
            // (Self::Led3 { .. }, Self::Led3 { .. }) => true,
            _ => false,
        }
    }
}
// impl Format for RGBInfo {
//     fn format(&self, fmt: defmt::Formatter) {
//         match self {
//             Self::Led0 { state } => match state {
//                 Some(state) => defmt::write!(fmt, "[{} {} {}]", state.r, state.g, state.b),
//                 None => defmt::write!(fmt, "None"),
//             },
//             Self::Led1 { state } => match state {
//                 Some(state) => defmt::write!(fmt, "[{} {} {}]", state.r, state.g, state.b),
//                 None => defmt::write!(fmt, "None"),
//             },
//             Self::Led2 { state } => match state {
//                 Some(state) => defmt::write!(fmt, "[{} {} {}]", state.r, state.g, state.b),
//                 None => defmt::write!(fmt, "None"),
//             },
//             Self::Led3 { state } => match state {
//                 Some(state) => defmt::write!(fmt, "[{} {} {}]", state.r, state.g, state.b),
//                 None => defmt::write!(fmt, "None"),
//             },
//         }
//     }
// }
// Stores the state of liquid fuel sources
#[derive(Debug, Clone, PartialEq, Format)]
pub enum FuelInfo {
    Water { state: f32 },
}
impl FuelInfo {
    pub fn try_get(&self) -> Result<&f32, DataError> {
        Ok(match self {
            FuelInfo::Water { state } => state,
        })
    }
}
impl Display for FuelInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Water { state } => {
                let num = if state > &100_f32 {
                    100_f32
                } else if state < &0_f32 {
                    0_f32
                } else {
                    state.clone()
                };
                f.write_fmt(format_args!("{:.0}", num))
            }
        }
    }
}
impl Deq for FuelInfo {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (FuelInfo::Water { .. }, FuelInfo::Water { .. }) => true,
        }
    }
}
#[derive(Debug, Clone, PartialEq, Format)]
pub enum BatteryInfo {
    Soc { state: f32 },
    Voltage { state: f32 },
    Current { state: f32 },
    Temperature { state: f32 },
}
impl Display for BatteryInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Soc { state } => f.write_fmt(format_args!("{}", state)),
            Self::Current { state } => f.write_fmt(format_args!("{}", state)),
            Self::Temperature { state } => f.write_fmt(format_args!("{}", state)),
            Self::Voltage { state } => f.write_fmt(format_args!("{}", state)),
        }
    }
}
impl Deq for BatteryInfo {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (BatteryInfo::Soc { .. }, BatteryInfo::Soc { .. }) => true,
            (BatteryInfo::Voltage { .. }, BatteryInfo::Voltage { .. }) => true,
            (BatteryInfo::Current { .. }, BatteryInfo::Current { .. }) => true,
            (BatteryInfo::Temperature { .. }, BatteryInfo::Temperature { .. }) => true,
            _ => false,
        }
    }
}
#[derive(Debug, Clone, PartialEq, Default, Format)]
pub struct Imu {
    pub roll: f32,
    pub pitch: f32,
}
#[derive(Debug, Clone, PartialEq, Format)]
pub enum IMUInfo {
    Angles { state: Imu },
    Zero,
}
impl Display for IMUInfo {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            Self::Angles { state } => {
                f.write_fmt(format_args!("r{:.2} x p{:.2}", state.roll, state.pitch))
            }
            Self::Zero => f.write_str("Zero"),
        }
    }
}
impl Deq for IMUInfo {
    fn deq(&self, rhs: &Self) -> bool {
        match (self, rhs) {
            (IMUInfo::Angles { .. }, IMUInfo::Angles { .. }) => true,
            (IMUInfo::Zero { .. }, IMUInfo::Zero { .. }) => true,
            _ => false,
        }
    }
}
pub enum Msg<T> {
    State { state: T },
    Toggle { field: T }, // Acts as Zero command for IMU
    NoActionUpdate,      // Mean tto be used when refreshing a page, resends all info
}

// if let DataValue::RGBLed { f } = state {
//     let resp = DataValue::RGBLed {
//         f: match f {
//             RGBInfo::Led0 { .. } => {
//                 let mode = led0.get();
//                 led0.set(!mode);
//                 RGBInfo::Led0 {
//                     mode: led0.get_mode(),
//                 }
//             }
//             RGBInfo::Led1 { .. } => {
//                 let mode = led1.get();
//                 led1.set(!mode);
//                 RGBInfo::Led1 {
//                     mode: led1.get_mode(),
//                 }
//             }
//             // RGBInfo::Led2 { mode } => RGBInfo::Led2 {
//             //     mode: led2.set(mode).await,
//             // },
//             // RGBInfo::Led3 { mode } => RGBInfo::Led3 {
//             //     mode: led3.set(mode).await,
//             // },
//         },
//     };
//     pubs.send(Events::ReDraw { id: Some(resp) }).await;
// }
