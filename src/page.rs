use defmt::{info, Format};
use embassy_futures::block_on;
use heapless::Vec;

use crate::{
    components::{Click, Component},
    datastore::DataValue,
    settings::Settings::Constants,
};

#[derive(Clone, Debug, Format)]
pub enum WheelAction {
    Down,
    Up,
    Click,
    Long,
}
#[derive(Clone, Debug, Format, PartialEq)]
pub enum Pages {
    Home,
    Buttons,
    Battery,
    Water,
    RgbLed,
    Imu,
    Zero,
}
pub struct Page {
    elements: Vec<Component, { Constants::N_PAGE_ELEMENTS }>,
    selectables: Vec<bool, { Constants::N_PAGE_ELEMENTS }>,
    last: usize,
    target: usize,
}

impl Page {
    pub fn new() -> Self {
        Page {
            elements: Vec::new(),
            selectables: Vec::new(),
            last: 0,
            target: 0,
        }
    }
    // pub fn enter() {
    //     // Start timer that requests updates
    // }
    // pub fn leave() {
    //     // Stops timers
    //     // Clears everything it needs to from screen
    // }
    // pub fn check_updates(&mut self) {
    //     // Relays check once
    //     // IMU check 10hz
    //     // Battery check every 1second
    //     // Each update thread needs to be able to tell the UI to check for updates in data

    //     Timer::after(Duration::from_millis(300)).await;
    //     for elem in self.data_channels {
    //         elem.send(Msg::NoActionUpdate).await;
    //     }
    // }
    pub fn register_elem(&mut self, element: Component) {
        self.selectables
            .push(match &element {
                Component::Button { b: _ } => true,
                Component::FuelGauge { b } => {
                    if b.selectable() {
                        true
                    } else {
                        false
                    }
                }
                Component::Label { b } => {
                    if b.selectable() {
                        true
                    } else {
                        false
                    }
                }
                Component::Header { b: _ } => true,
                Component::ButtonGroup { b: _ } => true,
                Component::ImuCanvas { b: _ } => true,
                Component::RGBLed { b: _ } => true,
            })
            .unwrap();
        self.elements
            .push(element)
            .expect("Too many elements to add to page");

        if *self.selectables.last().unwrap() {
            self.last = self.elements.len();
        }
    }
    fn select(&mut self, action: Click) {
        block_on(self.elements[self.target].event_handler(action));
    }
    pub fn event_handler(&mut self, cmd: WheelAction) {
        let event = match cmd {
            WheelAction::Down => {
                self.select(Click::None);
                while self.target < self.last - 1 {
                    self.target += 1;
                    if self.selectables[self.target] {
                        break;
                    }
                }
                Click::Hover
            }
            WheelAction::Up => {
                self.select(Click::None);
                while self.target > 0 {
                    self.target -= 1;
                    if self.selectables[self.target] {
                        break;
                    }
                }
                Click::Hover
            }
            WheelAction::Click => Click::Up,
            WheelAction::Long => Click::Long,
        };
        info!(
            "Selecting obj {}/{} on current page",
            self.target, self.last
        );
        self.select(event);
    }
    pub fn draw(&self, id: Option<DataValue>) {
        for e in &self.elements {
            e.draw(id.clone());
        }
    }
}
